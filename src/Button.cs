﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace InputController
{
    public sealed class Button
    {
        [DllImport("USER32.dll")]
        internal static extern short GetKeyState(VirtualKeys nVirtKey);

        private readonly VirtualKeys virtualKey;
        //private readonly int scanCode;
        private readonly Switch up, down;

        public string Name { get { return virtualKey.ToString(); } }
        public int VirtualCode { get { return (int)virtualKey; } }
        internal VirtualKeys VirtualKey { get { return virtualKey; } }
        //public int ScanCode { get { return scanCode; } }
        public Switch UpSwitch { get { return up; } }
        public Switch DownSwitch { get { return down; } }
        public bool IsMouseButton { get { return VirtualCode <= 0x7; } }
        public bool IsKeyboardButton { get { return VirtualCode > 0x7; } }

        private Button(VirtualKeys virtualKey)
        {
            this.virtualKey = virtualKey;
            //this.scanCode = virtualCode;
            bool isPressed = Convert.ToBoolean(GetKeyState(virtualKey) & 0x8000);
            this.up = new Switch(this, true, !isPressed);
            this.down = new Switch(this, false, isPressed);
        }

        public void ClearHotkeys() { up.ClearHotkeys(); down.ClearHotkeys(); }

        #region Button Instances
        public static readonly Button LeftButton = new Button(VirtualKeys.LeftButton);
        public static readonly Button RightButton = new Button(VirtualKeys.RightButton);
        public static readonly Button MouseWheelH = new Button(VirtualKeys.MouseWheelH);   //CUSTOM public static readonly Button Cancel = new Button(0x03);
        public static readonly Button MiddleButton = new Button(VirtualKeys.MiddleButton);
        public static readonly Button ExtraButton1 = new Button(VirtualKeys.ExtraButton1);
        public static readonly Button ExtraButton2 = new Button(VirtualKeys.ExtraButton2);
        public static readonly Button MouseWheelV = new Button(VirtualKeys.MouseWheelV);   //CUSTOM
        public static readonly Button Back = new Button(VirtualKeys.Back);
        public static readonly Button Tab = new Button(VirtualKeys.Tab);
        //public static readonly Button LineFeed = new Button(VirtualKeys.LineFeed);
        //public static readonly Button Clear = new Button(VirtualKeys.Clear);
        public static readonly Button Return = new Button(VirtualKeys.Return);
        //public static readonly Button Shift = new Button(VirtualKeys.Shift);
        //public static readonly Button Control = new Button(VirtualKeys.Control);
        //public static readonly Button Menu = new Button(VirtualKeys.Menu);
        public static readonly Button Pause = new Button(VirtualKeys.Pause);
        public static readonly Button CapsLock = new Button(VirtualKeys.CapsLock);
        //public static readonly Button Kana = new Button(VirtualKeys.Kana);
        //public static readonly Button Hangeul = new Button(VirtualKeys.Hangeul);
        //public static readonly Button Hangul = new Button(VirtualKeys.Hangul);
        //public static readonly Button Junja = new Button(VirtualKeys.Junja);
        //public static readonly Button Final = new Button(VirtualKeys.Final);
        //public static readonly Button Hanja = new Button(VirtualKeys.Hanja);
        //public static readonly Button Kanji = new Button(VirtualKeys.Kanji);
        public static readonly Button Escape = new Button(VirtualKeys.Escape);
        //public static readonly Button Convert = new Button(VirtualKeys.Convert);
        //public static readonly Button NonConvert = new Button(VirtualKeys.NonConvert);
        //public static readonly Button Accept = new Button(VirtualKeys.Accept);
        //public static readonly Button ModeChange = new Button(VirtualKeys.ModeChange);
        public static readonly Button Space = new Button(VirtualKeys.Space);
        public static readonly Button PageUp = new Button(VirtualKeys.PageUp);        //Prior
        public static readonly Button PageDown = new Button(VirtualKeys.PageDown);      //Next
        public static readonly Button End = new Button(VirtualKeys.End);
        public static readonly Button Home = new Button(VirtualKeys.Home);
        public static readonly Button Left = new Button(VirtualKeys.Left);
        public static readonly Button Up = new Button(VirtualKeys.Up);
        public static readonly Button Right = new Button(VirtualKeys.Right);
        public static readonly Button Down = new Button(VirtualKeys.Down);
        //public static readonly Button Select = new Button(VirtualKeys.Select);
        //public static readonly Button Print = new Button(VirtualKeys.Print);
        //public static readonly Button Execute = new Button(VirtualKeys.Execute);
        public static readonly Button PrintScreen = new Button(VirtualKeys.PrintScreen);    //Snapshot
        public static readonly Button Insert = new Button(VirtualKeys.Insert);
        public static readonly Button Delete = new Button(VirtualKeys.Delete);
        //public static readonly Button Help = new Button(VirtualKeys.Help);
        public static readonly Button N0 = new Button(VirtualKeys.N0);
        public static readonly Button N1 = new Button(VirtualKeys.N1);
        public static readonly Button N2 = new Button(VirtualKeys.N2);
        public static readonly Button N3 = new Button(VirtualKeys.N3);
        public static readonly Button N4 = new Button(VirtualKeys.N4);
        public static readonly Button N5 = new Button(VirtualKeys.N5);
        public static readonly Button N6 = new Button(VirtualKeys.N6);
        public static readonly Button N7 = new Button(VirtualKeys.N7);
        public static readonly Button N8 = new Button(VirtualKeys.N8);
        public static readonly Button N9 = new Button(VirtualKeys.N9);
        public static readonly Button A = new Button(VirtualKeys.A);
        public static readonly Button B = new Button(VirtualKeys.B);
        public static readonly Button C = new Button(VirtualKeys.C);
        public static readonly Button D = new Button(VirtualKeys.D);
        public static readonly Button E = new Button(VirtualKeys.E);
        public static readonly Button F = new Button(VirtualKeys.F);
        public static readonly Button G = new Button(VirtualKeys.G);
        public static readonly Button H = new Button(VirtualKeys.H);
        public static readonly Button I = new Button(VirtualKeys.I);
        public static readonly Button J = new Button(VirtualKeys.J);
        public static readonly Button K = new Button(VirtualKeys.K);
        public static readonly Button L = new Button(VirtualKeys.L);
        public static readonly Button M = new Button(VirtualKeys.M);
        public static readonly Button N = new Button(VirtualKeys.N);
        public static readonly Button O = new Button(VirtualKeys.O);
        public static readonly Button P = new Button(VirtualKeys.P);
        public static readonly Button Q = new Button(VirtualKeys.Q);
        public static readonly Button R = new Button(VirtualKeys.R);
        public static readonly Button S = new Button(VirtualKeys.S);
        public static readonly Button T = new Button(VirtualKeys.T);
        public static readonly Button U = new Button(VirtualKeys.U);
        public static readonly Button V = new Button(VirtualKeys.V);
        public static readonly Button W = new Button(VirtualKeys.W);
        public static readonly Button X = new Button(VirtualKeys.X);
        public static readonly Button Y = new Button(VirtualKeys.Y);
        public static readonly Button Z = new Button(VirtualKeys.Z);
        public static readonly Button LeftWindows = new Button(VirtualKeys.LeftWindows);
        public static readonly Button RightWindows = new Button(VirtualKeys.RightWindows);
        public static readonly Button Application = new Button(VirtualKeys.Application);
        //public static readonly Button Sleep = new Button(VirtualKeys.Sleep);
        public static readonly Button Numpad0 = new Button(VirtualKeys.Numpad0);
        public static readonly Button Numpad1 = new Button(VirtualKeys.Numpad1);
        public static readonly Button Numpad2 = new Button(VirtualKeys.Numpad2);
        public static readonly Button Numpad3 = new Button(VirtualKeys.Numpad3);
        public static readonly Button Numpad4 = new Button(VirtualKeys.Numpad4);
        public static readonly Button Numpad5 = new Button(VirtualKeys.Numpad5);
        public static readonly Button Numpad6 = new Button(VirtualKeys.Numpad6);
        public static readonly Button Numpad7 = new Button(VirtualKeys.Numpad7);
        public static readonly Button Numpad8 = new Button(VirtualKeys.Numpad8);
        public static readonly Button Numpad9 = new Button(VirtualKeys.Numpad9);
        public static readonly Button Multiply = new Button(VirtualKeys.Multiply);
        public static readonly Button Add = new Button(VirtualKeys.Add);
        //public static readonly Button Separator = new Button(VirtualKeys.Separator);
        public static readonly Button Subtract = new Button(VirtualKeys.Subtract);
        public static readonly Button Decimal = new Button(VirtualKeys.Decimal);
        public static readonly Button Divide = new Button(VirtualKeys.Divide);
        public static readonly Button F1 = new Button(VirtualKeys.F1);
        public static readonly Button F2 = new Button(VirtualKeys.F2);
        public static readonly Button F3 = new Button(VirtualKeys.F3);
        public static readonly Button F4 = new Button(VirtualKeys.F4);
        public static readonly Button F5 = new Button(VirtualKeys.F5);
        public static readonly Button F6 = new Button(VirtualKeys.F6);
        public static readonly Button F7 = new Button(VirtualKeys.F7);
        public static readonly Button F8 = new Button(VirtualKeys.F8);
        public static readonly Button F9 = new Button(VirtualKeys.F9);
        public static readonly Button F10 = new Button(VirtualKeys.F10);
        public static readonly Button F11 = new Button(VirtualKeys.F11);
        public static readonly Button F12 = new Button(VirtualKeys.F12);
        //public static readonly Button F13 = new Button(VirtualKeys.F13);
        //public static readonly Button F14 = new Button(VirtualKeys.F14);
        //public static readonly Button F15 = new Button(VirtualKeys.F15);
        //public static readonly Button F16 = new Button(VirtualKeys.F16);
        //public static readonly Button F17 = new Button(VirtualKeys.F17);
        //public static readonly Button F18 = new Button(VirtualKeys.F18);
        //public static readonly Button F19 = new Button(VirtualKeys.F19);
        //public static readonly Button F20 = new Button(VirtualKeys.F20);
        //public static readonly Button F21 = new Button(VirtualKeys.F21);
        //public static readonly Button F22 = new Button(VirtualKeys.F22);
        //public static readonly Button F23 = new Button(VirtualKeys.F23);
        //public static readonly Button F24 = new Button(VirtualKeys.F24);
        public static readonly Button NumLock = new Button(VirtualKeys.NumLock);
        public static readonly Button ScrollLock = new Button(VirtualKeys.ScrollLock);
        //public static readonly Button NEC_Equal = new Button(VirtualKeys.NEC_Equal);
        //public static readonly Button Fujitsu_Jisho = new Button(VirtualKeys.Fujitsu_Jisho);
        //public static readonly Button Fujitsu_Masshou = new Button(VirtualKeys.Fujitsu_Masshou);
        //public static readonly Button Fujitsu_Touroku = new Button(VirtualKeys.Fujitsu_Touroku);
        //public static readonly Button Fujitsu_Loya = new Button(VirtualKeys.Fujitsu_Loya);
        //public static readonly Button Fujitsu_Roya = new Button(VirtualKeys.Fujitsu_Roya);
        public static readonly Button LeftShift = new Button(VirtualKeys.LeftShift);
        public static readonly Button RightShift = new Button(VirtualKeys.RightShift);
        public static readonly Button LeftControl = new Button(VirtualKeys.LeftControl);
        public static readonly Button RightControl = new Button(VirtualKeys.RightControl);
        public static readonly Button LeftAlt = new Button(VirtualKeys.LeftAlt);       //LeftMenu
        public static readonly Button RightAlt = new Button(VirtualKeys.RightAlt);      //RightMenu
        //public static readonly Button BrowserBack = new Button(VirtualKeys.BrowserBack);
        //public static readonly Button BrowserForward = new Button(VirtualKeys.BrowserForward);
        //public static readonly Button BrowserRefresh = new Button(VirtualKeys.BrowserRefresh);
        //public static readonly Button BrowserStop = new Button(VirtualKeys.BrowserStop);
        //public static readonly Button BrowserSearch = new Button(VirtualKeys.BrowserSearch);
        //public static readonly Button BrowserFavorites = new Button(VirtualKeys.BrowserFavorites);
        //public static readonly Button BrowserHome = new Button(VirtualKeys.BrowserHome);
        //public static readonly Button VolumeMute = new Button(VirtualKeys.VolumeMute);
        //public static readonly Button VolumeDown = new Button(VirtualKeys.VolumeDown);
        //public static readonly Button VolumeUp = new Button(VirtualKeys.VolumeUp);
        //public static readonly Button MediaNextTrack = new Button(VirtualKeys.MediaNextTrack);
        //public static readonly Button MediaPrevTrack = new Button(VirtualKeys.MediaPrevTrack);
        //public static readonly Button MediaStop = new Button(VirtualKeys.MediaStop);
        //public static readonly Button MediaPlayPause = new Button(VirtualKeys.MediaPlayPause);
        //public static readonly Button LaunchMail = new Button(VirtualKeys.LaunchMail);
        //public static readonly Button LaunchMediaSelect = new Button(VirtualKeys.LaunchMediaSelect);
        //public static readonly Button LaunchApplication1 = new Button(VirtualKeys.LaunchApplication1);
        //public static readonly Button LaunchApplication2 = new Button(VirtualKeys.LaunchApplication2);
        public static readonly Button Colon = new Button(VirtualKeys.Colon);         //OEM1
        public static readonly Button Plus = new Button(VirtualKeys.Plus);          //OEMPlus
        public static readonly Button Comma = new Button(VirtualKeys.Comma);         //OEMComma
        public static readonly Button Minus = new Button(VirtualKeys.Minus);         //OEMMinus
        public static readonly Button Period = new Button(VirtualKeys.Period);        //OEMPeriod
        public static readonly Button Question = new Button(VirtualKeys.Question);      //OEM2
        public static readonly Button Tilde = new Button(VirtualKeys.Tilde);         //OEM3
        public static readonly Button OpenBrackets = new Button(VirtualKeys.OpenBrackets);  //OEM4
        public static readonly Button Pipe = new Button(VirtualKeys.Pipe);          //OEM5
        public static readonly Button CloseBrackets = new Button(VirtualKeys.CloseBrackets); //OEM6
        public static readonly Button Quotes = new Button(VirtualKeys.Quotes);        //OEM7
        //public static readonly Button OEM8 = new Button(VirtualKeys.OEM8);
        //public static readonly Button OEMAX = new Button(VirtualKeys.OEMAX);
        //public static readonly Button OEM102 = new Button(VirtualKeys.OEM102);
        //public static readonly Button ICOHelp = new Button(VirtualKeys.ICOHelp);
        //public static readonly Button ICO00 = new Button(VirtualKeys.ICO00);
        //public static readonly Button ProcessKey = new Button(VirtualKeys.ProcessKey);
        //public static readonly Button ICOClear = new Button(VirtualKeys.ICOClear);
        //public static readonly Button Packet = new Button(VirtualKeys.Packet);
        //public static readonly Button OEMReset = new Button(VirtualKeys.OEMReset);
        //public static readonly Button OEMJump = new Button(VirtualKeys.OEMJump);
        //public static readonly Button OEMPA1 = new Button(VirtualKeys.OEMPA1);
        //public static readonly Button OEMPA2 = new Button(VirtualKeys.OEMPA2);
        //public static readonly Button OEMPA3 = new Button(VirtualKeys.OEMPA3);
        //public static readonly Button OEMWSCtrl = new Button(VirtualKeys.OEMWSCtrl);
        //public static readonly Button OEMCUSel = new Button(VirtualKeys.OEMCUSel);
        //public static readonly Button OEMATTN = new Button(VirtualKeys.OEMATTN);
        //public static readonly Button OEMFinish = new Button(VirtualKeys.OEMFinish);
        //public static readonly Button OEMCopy = new Button(VirtualKeys.OEMCopy);
        //public static readonly Button OEMAuto = new Button(VirtualKeys.OEMAuto);
        //public static readonly Button OEMENLW = new Button(VirtualKeys.OEMENLW);
        //public static readonly Button OEMBackTab = new Button(VirtualKeys.OEMBackTab);
        //public static readonly Button ATTN = new Button(VirtualKeys.ATTN);
        //public static readonly Button CRSel = new Button(VirtualKeys.CRSel);
        //public static readonly Button EXSel = new Button(VirtualKeys.EXSel);
        //public static readonly Button EREOF = new Button(VirtualKeys.EREOF);
        //public static readonly Button Play = new Button(VirtualKeys.Play);
        //public static readonly Button Zoom = new Button(VirtualKeys.Zoom);
        //public static readonly Button Noname = new Button(VirtualKeys.Noname);
        //public static readonly Button PA1 = new Button(VirtualKeys.PA1);
        //public static readonly Button OEMClear = new Button(VirtualKeys.OEMClear);
        #endregion
        #region VirtualKeys Enum
        internal enum VirtualKeys : ushort
        {
            LeftButton = 0x01,
            RightButton = 0x02,
            MouseWheelH = 0x03,   //CUSTOM public static readonly Button Cancel = new Button(0x03);
            MiddleButton = 0x04,
            ExtraButton1 = 0x05,
            ExtraButton2 = 0x06,
            MouseWheelV = 0x07,   //CUSTOM
            Back = 0x08,
            Tab = 0x09,
            //LineFeed = 0x0A,
            //Clear = 0x0C,
            Return = 0x0D,
            //Shift = 0x10,
            //Control = 0x11,
            //Menu = 0x12,
            Pause = 0x13,
            CapsLock = 0x14,
            //Kana = 0x15,
            //Hangeul = 0x15,
            //Hangul = 0x15,
            //Junja = 0x17,
            //Final = 0x18,
            //Hanja = 0x19,
            //Kanji = 0x19,
            Escape = 0x1B,
            //Convert = 0x1C,
            //NonConvert = 0x1D,
            //Accept = 0x1E,
            //ModeChange = 0x1F,
            Space = 0x20,
            PageUp = 0x21,        //Prior
            PageDown = 0x22,      //Next
            End = 0x23,
            Home = 0x24,
            Left = 0x25,
            Up = 0x26,
            Right = 0x27,
            Down = 0x28,
            //Select = 0x29,
            //Print = 0x2A,
            //Execute = 0x2B,
            PrintScreen = 0x2C,    //Snapshot
            Insert = 0x2D,
            Delete = 0x2E,
            //Help = 0x2F,
            N0 = 0x30,
            N1 = 0x31,
            N2 = 0x32,
            N3 = 0x33,
            N4 = 0x34,
            N5 = 0x35,
            N6 = 0x36,
            N7 = 0x37,
            N8 = 0x38,
            N9 = 0x39,
            A = 0x41,
            B = 0x42,
            C = 0x43,
            D = 0x44,
            E = 0x45,
            F = 0x46,
            G = 0x47,
            H = 0x48,
            I = 0x49,
            J = 0x4A,
            K = 0x4B,
            L = 0x4C,
            M = 0x4D,
            N = 0x4E,
            O = 0x4F,
            P = 0x50,
            Q = 0x51,
            R = 0x52,
            S = 0x53,
            T = 0x54,
            U = 0x55,
            V = 0x56,
            W = 0x57,
            X = 0x58,
            Y = 0x59,
            Z = 0x5A,
            LeftWindows = 0x5B,
            RightWindows = 0x5C,
            Application = 0x5D,
            //Sleep = 0x5F,
            Numpad0 = 0x60,
            Numpad1 = 0x61,
            Numpad2 = 0x62,
            Numpad3 = 0x63,
            Numpad4 = 0x64,
            Numpad5 = 0x65,
            Numpad6 = 0x66,
            Numpad7 = 0x67,
            Numpad8 = 0x68,
            Numpad9 = 0x69,
            Multiply = 0x6A,
            Add = 0x6B,
            //Separator = 0x6C,
            Subtract = 0x6D,
            Decimal = 0x6E,
            Divide = 0x6F,
            F1 = 0x70,
            F2 = 0x71,
            F3 = 0x72,
            F4 = 0x73,
            F5 = 0x74,
            F6 = 0x75,
            F7 = 0x76,
            F8 = 0x77,
            F9 = 0x78,
            F10 = 0x79,
            F11 = 0x7A,
            F12 = 0x7B,
            //F13 = 0x7C,
            //F14 = 0x7D,
            //F15 = 0x7E,
            //F16 = 0x7F,
            //F17 = 0x80,
            //F18 = 0x81,
            //F19 = 0x82,
            //F20 = 0x83,
            //F21 = 0x84,
            //F22 = 0x85,
            //F23 = 0x86,
            //F24 = 0x87,
            NumLock = 0x90,
            ScrollLock = 0x91,
            //NEC_Equal = 0x92,
            //Fujitsu_Jisho = 0x92,
            //Fujitsu_Masshou = 0x93,
            //Fujitsu_Touroku = 0x94,
            //Fujitsu_Loya = 0x95,
            //Fujitsu_Roya = 0x96,
            LeftShift = 0xA0,
            RightShift = 0xA1,
            LeftControl = 0xA2,
            RightControl = 0xA3,
            LeftAlt = 0xA4,       //LeftMenu
            RightAlt = 0xA5,      //RightMenu
            //BrowserBack = 0xA6,
            //BrowserForward = 0xA7,
            //BrowserRefresh = 0xA8,
            //BrowserStop = 0xA9,
            //BrowserSearch = 0xAA,
            //BrowserFavorites = 0xAB,
            //BrowserHome = 0xAC,
            //VolumeMute = 0xAD,
            //VolumeDown = 0xAE,
            //VolumeUp = 0xAF,
            //MediaNextTrack = 0xB0,
            //MediaPrevTrack = 0xB1,
            //MediaStop = 0xB2,
            //MediaPlayPause = 0xB3,
            //LaunchMail = 0xB4,
            //LaunchMediaSelect = 0xB5,
            //LaunchApplication1 = 0xB6,
            //LaunchApplication2 = 0xB7,
            Colon = 0xBA,         //OEM1
            Plus = 0xBB,          //OEMPlus
            Comma = 0xBC,         //OEMComma
            Minus = 0xBD,         //OEMMinus
            Period = 0xBE,        //OEMPeriod
            Question = 0xBF,      //OEM2
            Tilde = 0xC0,         //OEM3
            OpenBrackets = 0xDB,  //OEM4
            Pipe = 0xDC,          //OEM5
            CloseBrackets = 0xDD, //OEM6
            Quotes = 0xDE,        //OEM7
            //OEM8 = 0xDF,
            //OEMAX = 0xE1,
            //OEM102 = 0xE2,
            //ICOHelp = 0xE3,
            //ICO00 = 0xE4,
            //ProcessKey = 0xE5,
            //ICOClear = 0xE6,
            //Packet = 0xE7,
            //OEMReset = 0xE9,
            //OEMJump = 0xEA,
            //OEMPA1 = 0xEB,
            //OEMPA2 = 0xEC,
            //OEMPA3 = 0xED,
            //OEMWSCtrl = 0xEE,
            //OEMCUSel = 0xEF,
            //OEMATTN = 0xF0,
            //OEMFinish = 0xF1,
            //OEMCopy = 0xF2,
            //OEMAuto = 0xF3,
            //OEMENLW = 0xF4,
            //OEMBackTab = 0xF5,
            //ATTN = 0xF6,
            //CRSel = 0xF7,
            //EXSel = 0xF8,
            //EREOF = 0xF9,
            //Play = 0xFA,
            //Zoom = 0xFB,
            //Noname = 0xFC,
            //PA1 = 0xFD,
            //OEMClear = 0xFE,
        }
        #endregion
        #region Button Collection
        //public static readonly ReadOnlyCollection<Button> Buttons;
        public static readonly ReadOnlyDictionary<int, Button> ButtonIntLookup;
        public static readonly ReadOnlyDictionary<string, Button> ButtonNameLookup;
        static Button()
        {
            //Button[] buttonList = new Button[(int)Enum.GetValues(typeof(VirtualKeys)).Cast<VirtualKeys>().Last() + 1];
            Dictionary<int, Button> intDictionary = new Dictionary<int, Button>();
            Dictionary<string, Button> nameDictionary = new Dictionary<string, Button>();
            
            /*buttonList[0x01] = LeftButton;*/ intDictionary.Add(LeftButton.VirtualCode, LeftButton); nameDictionary.Add(LeftButton.Name, LeftButton); 
            /*buttonList[0x02] = RightButton; */intDictionary.Add(RightButton.VirtualCode, RightButton); nameDictionary.Add(RightButton.Name, RightButton);
            /*buttonList[0x03] = MouseWheelH; */intDictionary.Add(MouseWheelH.VirtualCode, MouseWheelH); nameDictionary.Add(MouseWheelH.Name, MouseWheelH);   //CUSTOM public static readonly Button Cancel = new Button(0x03);
            /*buttonList[0x04] = MiddleButton; */intDictionary.Add(MiddleButton.VirtualCode, MiddleButton); nameDictionary.Add(MiddleButton.Name, MiddleButton);
            /*buttonList[0x05] = ExtraButton1; */intDictionary.Add(ExtraButton1.VirtualCode, ExtraButton1); nameDictionary.Add(ExtraButton1.Name, ExtraButton1);
            /*buttonList[0x06] = ExtraButton2; */intDictionary.Add(ExtraButton2.VirtualCode, ExtraButton2); nameDictionary.Add(ExtraButton2.Name, ExtraButton2);
            /*buttonList[0x07] = MouseWheelV; */intDictionary.Add(MouseWheelV.VirtualCode, MouseWheelV); nameDictionary.Add(MouseWheelV.Name, MouseWheelV);   //CUSTOM
            /*buttonList[0x08] = Back; */intDictionary.Add(Back.VirtualCode, Back); nameDictionary.Add(Back.Name, Back);
            /*buttonList[0x09] = Tab; */intDictionary.Add(Tab.VirtualCode, Tab); nameDictionary.Add(Tab.Name, Tab);
            ///*buttonList[0x0A] = LineFeed; */intDictionary.Add(LineFeed.VirtualCode, LineFeed); buttonDictionary.Add(LineFeed.Name, LineFeed);
            ///*buttonList[0x0C] = Clear; */intDictionary.Add(Clear.VirtualCode, Clear); buttonDictionary.Add(Clear.Name, Clear);
            /*buttonList[0x0D] = Return; */intDictionary.Add(Return.VirtualCode, Return); nameDictionary.Add(Return.Name, Return);
            ///*buttonList[0x10] = Shift; */intDictionary.Add(Shift.VirtualCode, Shift); buttonDictionary.Add(Shift.Name, Shift);
            ///*buttonList[0x11] = Control; */intDictionary.Add(Control.VirtualCode, Control); buttonDictionary.Add(Control.Name, Control);
            ///*buttonList[0x12] = Menu; */intDictionary.Add(Menu.VirtualCode, Menu); buttonDictionary.Add(Menu.Name, Menu);
            /*buttonList[0x13] = Pause; */intDictionary.Add(Pause.VirtualCode, Pause); nameDictionary.Add(Pause.Name, Pause);
            /*buttonList[0x14] = CapsLock; */intDictionary.Add(CapsLock.VirtualCode, CapsLock); nameDictionary.Add(CapsLock.Name, CapsLock);
            ///*buttonList[0x15] = Kana; */intDictionary.Add(Kana.VirtualCode, Kana); buttonDictionary.Add(Kana.Name, Kana);
            ///*buttonList[0x15] = Hangeul; */intDictionary.Add(Hangeul.VirtualCode, Hangeul); buttonDictionary.Add(Hangeul.Name, Hangeul);
            ///*buttonList[0x15] = Hangul; */intDictionary.Add(Hangul.VirtualCode, Hangul); buttonDictionary.Add(Hangul.Name, Hangul);
            ///*buttonList[0x17] = Junja; */intDictionary.Add(Junja.VirtualCode, Junja); buttonDictionary.Add(Junja.Name, Junja);
            ///*buttonList[0x18] = Final; */intDictionary.Add(Final.VirtualCode, Final); buttonDictionary.Add(Final.Name, Final);
            ///*buttonList[0x19] = Hanja; */intDictionary.Add(Hanja.VirtualCode, Hanja); buttonDictionary.Add(Hanja.Name, Hanja);
            ///*buttonList[0x19] = Kanji; */intDictionary.Add(Kanji.VirtualCode, Kanji); buttonDictionary.Add(Kanji.Name, Kanji);
            /*buttonList[0x1B] = Escape; */intDictionary.Add(Escape.VirtualCode, Escape); nameDictionary.Add(Escape.Name, Escape);
            ///*buttonList[0x1C] = Convert; */intDictionary.Add(Convert.VirtualCode, Convert); buttonDictionary.Add(Convert.Name, Convert);
            ///*buttonList[0x1D] = NonConvert; */intDictionary.Add(NonConvert.VirtualCode, NonConvert); buttonDictionary.Add(NonConvert.Name, NonConvert);
            ///*buttonList[0x1E] = Accept; */intDictionary.Add(Accept.VirtualCode, Accept); buttonDictionary.Add(Accept.Name, Accept);
            ///*buttonList[0x1F] = ModeChange; */intDictionary.Add(ModeChange.VirtualCode, ModeChange); buttonDictionary.Add(ModeChange.Name, ModeChange);
            /*buttonList[0x20] = Space; */intDictionary.Add(Space.VirtualCode, Space); nameDictionary.Add(Space.Name, Space);
            /*buttonList[0x21] = PageUp; */intDictionary.Add(PageUp.VirtualCode, PageUp); nameDictionary.Add(PageUp.Name, PageUp);        //Prior
            /*buttonList[0x22] = PageDown; */intDictionary.Add(PageDown.VirtualCode, PageDown); nameDictionary.Add(PageDown.Name, PageDown);      //Next
            /*buttonList[0x23] = End; */intDictionary.Add(End.VirtualCode, End); nameDictionary.Add(End.Name, End);
            /*buttonList[0x24] = Home; */intDictionary.Add(Home.VirtualCode, Home); nameDictionary.Add(Home.Name, Home);
            /*buttonList[0x25] = Left; */intDictionary.Add(Left.VirtualCode, Left); nameDictionary.Add(Left.Name, Left);
            /*buttonList[0x26] = Up; */intDictionary.Add(Up.VirtualCode, Up); nameDictionary.Add(Up.Name, Up);
            /*buttonList[0x27] = Right; */intDictionary.Add(Right.VirtualCode, Right); nameDictionary.Add(Right.Name, Right);
            /*buttonList[0x28] = Down; */intDictionary.Add(Down.VirtualCode, Down); nameDictionary.Add(Down.Name, Down);
            ///*buttonList[0x29] = Select; */intDictionary.Add(Select.VirtualCode, Select); buttonDictionary.Add(Select.Name, Select);
            ///*buttonList[0x2A] = Print; */intDictionary.Add(Print.VirtualCode, Print); buttonDictionary.Add(Print.Name, Print);
            ///*buttonList[0x2B] = Execute; */intDictionary.Add(Execute.VirtualCode, Execute); buttonDictionary.Add(Execute.Name, Execute);
            /*buttonList[0x2C] = PrintScreen; */intDictionary.Add(PrintScreen.VirtualCode, PrintScreen); nameDictionary.Add(PrintScreen.Name, PrintScreen);    //Snapshot
            /*buttonList[0x2D] = Insert; */intDictionary.Add(Insert.VirtualCode, Insert); nameDictionary.Add(Insert.Name, Insert);
            /*buttonList[0x2E] = Delete; */intDictionary.Add(Delete.VirtualCode, Delete); nameDictionary.Add(Delete.Name, Delete);
            ///*buttonList[0x2F] = Help; */intDictionary.Add(Help.VirtualCode, Help); buttonDictionary.Add(Help.Name, Help);
            /*buttonList[0x30] = N0; */intDictionary.Add(N0.VirtualCode, N0); nameDictionary.Add(N0.Name, N0);
            /*buttonList[0x31] = N1; */intDictionary.Add(N1.VirtualCode, N1); nameDictionary.Add(N1.Name, N1);
            /*buttonList[0x32] = N2; */intDictionary.Add(N2.VirtualCode, N2); nameDictionary.Add(N2.Name, N2);
            /*buttonList[0x33] = N3; */intDictionary.Add(N3.VirtualCode, N3); nameDictionary.Add(N3.Name, N3);
            /*buttonList[0x34] = N4; */intDictionary.Add(N4.VirtualCode, N4); nameDictionary.Add(N4.Name, N4);
            /*buttonList[0x35] = N5; */intDictionary.Add(N5.VirtualCode, N5); nameDictionary.Add(N5.Name, N5);
            /*buttonList[0x36] = N6; */intDictionary.Add(N6.VirtualCode, N6); nameDictionary.Add(N6.Name, N6);
            /*buttonList[0x37] = N7; */intDictionary.Add(N7.VirtualCode, N7); nameDictionary.Add(N7.Name, N7);
            /*buttonList[0x38] = N8; */intDictionary.Add(N8.VirtualCode, N8); nameDictionary.Add(N8.Name, N8);
            /*buttonList[0x39] = N9; */intDictionary.Add(N9.VirtualCode, N9); nameDictionary.Add(N9.Name, N9);
            /*buttonList[0x41] = A; */intDictionary.Add(A.VirtualCode, A); nameDictionary.Add(A.Name, A);
            /*buttonList[0x42] = B; */intDictionary.Add(B.VirtualCode, B); nameDictionary.Add(B.Name, B);
            /*buttonList[0x43] = C; */intDictionary.Add(C.VirtualCode, C); nameDictionary.Add(C.Name, C);
            /*buttonList[0x44] = D; */intDictionary.Add(D.VirtualCode, D); nameDictionary.Add(D.Name, D);
            /*buttonList[0x45] = E; */intDictionary.Add(E.VirtualCode, E); nameDictionary.Add(E.Name, E);
            /*buttonList[0x46] = F; */intDictionary.Add(F.VirtualCode, F); nameDictionary.Add(F.Name, F);
            /*buttonList[0x47] = G; */intDictionary.Add(G.VirtualCode, G); nameDictionary.Add(G.Name, G);
            /*buttonList[0x48] = H; */intDictionary.Add(H.VirtualCode, H); nameDictionary.Add(H.Name, H);
            /*buttonList[0x49] = I; */intDictionary.Add(I.VirtualCode, I); nameDictionary.Add(I.Name, I);
            /*buttonList[0x4A] = J; */intDictionary.Add(J.VirtualCode, J); nameDictionary.Add(J.Name, J);
            /*buttonList[0x4B] = K; */intDictionary.Add(K.VirtualCode, K); nameDictionary.Add(K.Name, K);
            /*buttonList[0x4C] = L; */intDictionary.Add(L.VirtualCode, L); nameDictionary.Add(L.Name, L);
            /*buttonList[0x4D] = M; */intDictionary.Add(M.VirtualCode, M); nameDictionary.Add(M.Name, M);
            /*buttonList[0x4E] = N; */intDictionary.Add(N.VirtualCode, N); nameDictionary.Add(N.Name, N);
            /*buttonList[0x4F] = O; */intDictionary.Add(O.VirtualCode, O); nameDictionary.Add(O.Name, O);
            /*buttonList[0x50] = P; */intDictionary.Add(P.VirtualCode, P); nameDictionary.Add(P.Name, P);
            /*buttonList[0x51] = Q; */intDictionary.Add(Q.VirtualCode, Q); nameDictionary.Add(Q.Name, Q);
            /*buttonList[0x52] = R; */intDictionary.Add(R.VirtualCode, R); nameDictionary.Add(R.Name, R);
            /*buttonList[0x53] = S; */intDictionary.Add(S.VirtualCode, S); nameDictionary.Add(S.Name, S);
            /*buttonList[0x54] = T; */intDictionary.Add(T.VirtualCode, T); nameDictionary.Add(T.Name, T);
            /*buttonList[0x55] = U; */intDictionary.Add(U.VirtualCode, U); nameDictionary.Add(U.Name, U);
            /*buttonList[0x56] = V; */intDictionary.Add(V.VirtualCode, V); nameDictionary.Add(V.Name, V);
            /*buttonList[0x57] = W; */intDictionary.Add(W.VirtualCode, W); nameDictionary.Add(W.Name, W);
            /*buttonList[0x58] = X; */intDictionary.Add(X.VirtualCode, X); nameDictionary.Add(X.Name, X);
            /*buttonList[0x59] = Y; */intDictionary.Add(Y.VirtualCode, Y); nameDictionary.Add(Y.Name, Y);
            /*buttonList[0x5A] = Z; */intDictionary.Add(Z.VirtualCode, Z); nameDictionary.Add(Z.Name, Z);
            /*buttonList[0x5B] = LeftWindows; */intDictionary.Add(LeftWindows.VirtualCode, LeftWindows); nameDictionary.Add(LeftWindows.Name, LeftWindows);
            /*buttonList[0x5C] = RightWindows; */intDictionary.Add(RightWindows.VirtualCode, RightWindows); nameDictionary.Add(RightWindows.Name, RightWindows);
            /*buttonList[0x5D] = Application; */intDictionary.Add(Application.VirtualCode, Application); nameDictionary.Add(Application.Name, Application);
            ///*buttonList[0x5F] = Sleep; */intDictionary.Add(Sleep.VirtualCode, Sleep); buttonDictionary.Add(Sleep.Name, Sleep);
            /*buttonList[0x60] = Numpad0; */intDictionary.Add(Numpad0.VirtualCode, Numpad0); nameDictionary.Add(Numpad0.Name, Numpad0);
            /*buttonList[0x61] = Numpad1; */intDictionary.Add(Numpad1.VirtualCode, Numpad1); nameDictionary.Add(Numpad1.Name, Numpad1);
            /*buttonList[0x62] = Numpad2; */intDictionary.Add(Numpad2.VirtualCode, Numpad2); nameDictionary.Add(Numpad2.Name, Numpad2);
            /*buttonList[0x63] = Numpad3; */intDictionary.Add(Numpad3.VirtualCode, Numpad3); nameDictionary.Add(Numpad3.Name, Numpad3);
            /*buttonList[0x64] = Numpad4; */intDictionary.Add(Numpad4.VirtualCode, Numpad4); nameDictionary.Add(Numpad4.Name, Numpad4);
            /*buttonList[0x65] = Numpad5; */intDictionary.Add(Numpad5.VirtualCode, Numpad5); nameDictionary.Add(Numpad5.Name, Numpad5);
            /*buttonList[0x66] = Numpad6; */intDictionary.Add(Numpad6.VirtualCode, Numpad6); nameDictionary.Add(Numpad6.Name, Numpad6);
            /*buttonList[0x67] = Numpad7; */intDictionary.Add(Numpad7.VirtualCode, Numpad7); nameDictionary.Add(Numpad7.Name, Numpad7);
            /*buttonList[0x68] = Numpad8; */intDictionary.Add(Numpad8.VirtualCode, Numpad8); nameDictionary.Add(Numpad8.Name, Numpad8);
            /*buttonList[0x69] = Numpad9; */intDictionary.Add(Numpad9.VirtualCode, Numpad9); nameDictionary.Add(Numpad9.Name, Numpad9);
            /*buttonList[0x6A] = Multiply; */intDictionary.Add(Multiply.VirtualCode, Multiply); nameDictionary.Add(Multiply.Name, Multiply);
            /*buttonList[0x6B] = Add; */intDictionary.Add(Add.VirtualCode, Add); nameDictionary.Add(Add.Name, Add);
            ///*buttonList[0x6C] = Separator; */intDictionary.Add(Separator.VirtualCode, Separator); buttonDictionary.Add(Separator.Name, Separator);
            /*buttonList[0x6D] = Subtract; */intDictionary.Add(Subtract.VirtualCode, Subtract); nameDictionary.Add(Subtract.Name, Subtract);
            /*buttonList[0x6E] = Decimal; */intDictionary.Add(Decimal.VirtualCode, Decimal); nameDictionary.Add(Decimal.Name, Decimal);
            /*buttonList[0x6F] = Divide; */intDictionary.Add(Divide.VirtualCode, Divide); nameDictionary.Add(Divide.Name, Divide);
            /*buttonList[0x70] = F1; */intDictionary.Add(F1.VirtualCode, F1); nameDictionary.Add(F1.Name, F1);
            /*buttonList[0x71] = F2; */intDictionary.Add(F2.VirtualCode, F2); nameDictionary.Add(F2.Name, F2);
            /*buttonList[0x72] = F3; */intDictionary.Add(F3.VirtualCode, F3); nameDictionary.Add(F3.Name, F3);
            /*buttonList[0x73] = F4; */intDictionary.Add(F4.VirtualCode, F4); nameDictionary.Add(F4.Name, F4);
            /*buttonList[0x74] = F5; */intDictionary.Add(F5.VirtualCode, F5); nameDictionary.Add(F5.Name, F5);
            /*buttonList[0x75] = F6; */intDictionary.Add(F6.VirtualCode, F6); nameDictionary.Add(F6.Name, F6);
            /*buttonList[0x76] = F7; */intDictionary.Add(F7.VirtualCode, F7); nameDictionary.Add(F7.Name, F7);
            /*buttonList[0x77] = F8; */intDictionary.Add(F8.VirtualCode, F8); nameDictionary.Add(F8.Name, F8);
            /*buttonList[0x78] = F9; */intDictionary.Add(F9.VirtualCode, F9); nameDictionary.Add(F9.Name, F9);
            /*buttonList[0x79] = F10; */intDictionary.Add(F10.VirtualCode, F10); nameDictionary.Add(F10.Name, F10);
            /*buttonList[0x7A] = F11; */intDictionary.Add(F11.VirtualCode, F11); nameDictionary.Add(F11.Name, F11);
            /*buttonList[0x7B] = F12; */intDictionary.Add(F12.VirtualCode, F12); nameDictionary.Add(F12.Name, F12);
            ///*buttonList[0x7C] = F13; */intDictionary.Add(F13.VirtualCode, F13); buttonDictionary.Add(F13.Name, F13);
            ///*buttonList[0x7D] = F14; */intDictionary.Add(F14.VirtualCode, F14); buttonDictionary.Add(F14.Name, F14);
            ///*buttonList[0x7E] = F15; */intDictionary.Add(F15.VirtualCode, F15); buttonDictionary.Add(F15.Name, F15);
            ///*buttonList[0x7F] = F16; */intDictionary.Add(F16.VirtualCode, F16); buttonDictionary.Add(F16.Name, F16);
            ///*buttonList[0x80] = F17; */intDictionary.Add(F17.VirtualCode, F17); buttonDictionary.Add(F17.Name, F17);
            ///*buttonList[0x81] = F18; */intDictionary.Add(F18.VirtualCode, F18); buttonDictionary.Add(F18.Name, F18);
            ///*buttonList[0x82] = F19; */intDictionary.Add(F19.VirtualCode, F19); buttonDictionary.Add(F19.Name, F19);
            ///*buttonList[0x83] = F20; */intDictionary.Add(F20.VirtualCode, F20); buttonDictionary.Add(F20.Name, F20);
            ///*buttonList[0x84] = F21; */intDictionary.Add(F21.VirtualCode, F21); buttonDictionary.Add(F21.Name, F21);
            ///*buttonList[0x85] = F22; */intDictionary.Add(F22.VirtualCode, F22); buttonDictionary.Add(F22.Name, F22);
            ///*buttonList[0x86] = F23; */intDictionary.Add(F23.VirtualCode, F23); buttonDictionary.Add(F23.Name, F23);
            ///*buttonList[0x87] = F24; */intDictionary.Add(F24.VirtualCode, F24); buttonDictionary.Add(F24.Name, F24);
            /*buttonList[0x90] = NumLock; */intDictionary.Add(NumLock.VirtualCode, NumLock); nameDictionary.Add(NumLock.Name, NumLock);
            /*buttonList[0x91] = ScrollLock; */intDictionary.Add(ScrollLock.VirtualCode, ScrollLock); nameDictionary.Add(ScrollLock.Name, ScrollLock);
            ///*buttonList[0x92] = NEC_Equal; */intDictionary.Add(NEC_Equal.VirtualCode, NEC_Equal); buttonDictionary.Add(NEC_Equal.Name, NEC_Equal);
            ///*buttonList[0x92] = Fujitsu_Jisho; */intDictionary.Add(Fujitsu_Jisho.VirtualCode, Fujitsu_Jisho); buttonDictionary.Add(Fujitsu_Jisho.Name, Fujitsu_Jisho);
            ///*buttonList[0x93] = Fujitsu_Masshou; */intDictionary.Add(Fujitsu_Masshou.VirtualCode, Fujitsu_Masshou); buttonDictionary.Add(Fujitsu_Masshou.Name, Fujitsu_Masshou);
            ///*buttonList[0x94] = Fujitsu_Touroku; */intDictionary.Add(Fujitsu_Touroku.VirtualCode, Fujitsu_Touroku); buttonDictionary.Add(Fujitsu_Touroku.Name, Fujitsu_Touroku);
            ///*buttonList[0x95] = Fujitsu_Loya; */intDictionary.Add(Fujitsu_Loya.VirtualCode, Fujitsu_Loya); buttonDictionary.Add(Fujitsu_Loya.Name, Fujitsu_Loya);
            ///*buttonList[0x96] = Fujitsu_Roya; */intDictionary.Add(Fujitsu_Roya.VirtualCode, Fujitsu_Roya); buttonDictionary.Add(Fujitsu_Roya.Name, Fujitsu_Roya);
            /*buttonList[0xA0] = LeftShift; */intDictionary.Add(LeftShift.VirtualCode, LeftShift); nameDictionary.Add(LeftShift.Name, LeftShift);
            /*buttonList[0xA1] = RightShift; */intDictionary.Add(RightShift.VirtualCode, RightShift); nameDictionary.Add(RightShift.Name, RightShift);
            /*buttonList[0xA2] = LeftControl; */intDictionary.Add(LeftControl.VirtualCode, LeftControl); nameDictionary.Add(LeftControl.Name, LeftControl);
            /*buttonList[0xA3] = RightControl; */intDictionary.Add(RightControl.VirtualCode, RightControl); nameDictionary.Add(RightControl.Name, RightControl);
            /*buttonList[0xA4] = LeftAlt; */intDictionary.Add(LeftAlt.VirtualCode, LeftAlt); nameDictionary.Add(LeftAlt.Name, LeftAlt);       //LeftMenu
            /*buttonList[0xA5] = RightAlt; */intDictionary.Add(RightAlt.VirtualCode, RightAlt); nameDictionary.Add(RightAlt.Name, RightAlt);      //RightMenu
            ///*buttonList[0xA6] = BrowserBack; */intDictionary.Add(BrowserBack.VirtualCode, BrowserBack); buttonDictionary.Add(BrowserBack.Name, BrowserBack);
            ///*buttonList[0xA7] = BrowserForward; */intDictionary.Add(BrowserForward.VirtualCode, BrowserForward); buttonDictionary.Add(BrowserForward.Name, BrowserForward);
            ///*buttonList[0xA8] = BrowserRefresh; */intDictionary.Add(BrowserRefresh.VirtualCode, BrowserRefresh); buttonDictionary.Add(BrowserRefresh.Name, BrowserRefresh);
            ///*buttonList[0xA9] = BrowserStop; */intDictionary.Add(BrowserStop.VirtualCode, BrowserStop); buttonDictionary.Add(BrowserStop.Name, BrowserStop);
            ///*buttonList[0xAA] = BrowserSearch; */intDictionary.Add(BrowserSearch.VirtualCode, BrowserSearch); buttonDictionary.Add(BrowserSearch.Name, BrowserSearch);
            ///*buttonList[0xAB] = BrowserFavorites; */intDictionary.Add(BrowserFavorites.VirtualCode, BrowserFavorites); buttonDictionary.Add(BrowserFavorites.Name, BrowserFavorites);
            ///*buttonList[0xAC] = BrowserHome; */intDictionary.Add(BrowserHome.VirtualCode, BrowserHome); buttonDictionary.Add(BrowserHome.Name, BrowserHome);
            ///*buttonList[0xAD] = VolumeMute; */intDictionary.Add(VolumeMute.VirtualCode, VolumeMute); buttonDictionary.Add(VolumeMute.Name, VolumeMute);
            ///*buttonList[0xAE] = VolumeDown; */intDictionary.Add(VolumeDown.VirtualCode, VolumeDown); buttonDictionary.Add(VolumeDown.Name, VolumeDown);
            ///*buttonList[0xAF] = VolumeUp; */intDictionary.Add(VolumeUp.VirtualCode, VolumeUp); buttonDictionary.Add(VolumeUp.Name, VolumeUp);
            ///*buttonList[0xB0] = MediaNextTrack; */intDictionary.Add(MediaNextTrack.VirtualCode, MediaNextTrack); buttonDictionary.Add(MediaNextTrack.Name, MediaNextTrack);
            ///*buttonList[0xB1] = MediaPrevTrack; */intDictionary.Add(MediaPrevTrack.VirtualCode, MediaPrevTrack); buttonDictionary.Add(MediaPrevTrack.Name, MediaPrevTrack);
            ///*buttonList[0xB2] = MediaStop; */intDictionary.Add(MediaStop.VirtualCode, MediaStop); buttonDictionary.Add(MediaStop.Name, MediaStop);
            ///*buttonList[0xB3] = MediaPlayPause; */intDictionary.Add(MediaPlayPause.VirtualCode, MediaPlayPause); buttonDictionary.Add(MediaPlayPause.Name, MediaPlayPause);
            ///*buttonList[0xB4] = LaunchMail; */intDictionary.Add(LaunchMail.VirtualCode, LaunchMail); buttonDictionary.Add(LaunchMail.Name, LaunchMail);
            ///*buttonList[0xB5] = LaunchMediaSelect; */intDictionary.Add(LaunchMediaSelect.VirtualCode, LaunchMediaSelect); buttonDictionary.Add(LaunchMediaSelect.Name, LaunchMediaSelect);
            ///*buttonList[0xB6] = LaunchApplication1; */intDictionary.Add(LaunchApplication1.VirtualCode, LaunchApplication1); buttonDictionary.Add(LaunchApplication1.Name, LaunchApplication1);
            ///*buttonList[0xB7] = LaunchApplication2; */intDictionary.Add(LaunchApplication2.VirtualCode, LaunchApplication2); buttonDictionary.Add(LaunchApplication2.Name, LaunchApplication2);
            /*buttonList[0xBA] = Colon; */intDictionary.Add(Colon.VirtualCode, Colon); nameDictionary.Add(Colon.Name, Colon);         //OEM1
            /*buttonList[0xBB] = Plus; */intDictionary.Add(Plus.VirtualCode, Plus); nameDictionary.Add(Plus.Name, Plus);          //OEMPlus
            /*buttonList[0xBC] = Comma; */intDictionary.Add(Comma.VirtualCode, Comma); nameDictionary.Add(Comma.Name, Comma);         //OEMComma
            /*buttonList[0xBD] = Minus; */intDictionary.Add(Minus.VirtualCode, Minus); nameDictionary.Add(Minus.Name, Minus);         //OEMMinus
            /*buttonList[0xBE] = Period; */intDictionary.Add(Period.VirtualCode, Period); nameDictionary.Add(Period.Name, Period);        //OEMPeriod
            /*buttonList[0xBF] = Question; */intDictionary.Add(Question.VirtualCode, Question); nameDictionary.Add(Question.Name, Question);      //OEM2
            /*buttonList[0xC0] = Tilde; */intDictionary.Add(Tilde.VirtualCode, Tilde); nameDictionary.Add(Tilde.Name, Tilde);         //OEM3
            /*buttonList[0xDB] = OpenBrackets; */intDictionary.Add(OpenBrackets.VirtualCode, OpenBrackets); nameDictionary.Add(OpenBrackets.Name, OpenBrackets);  //OEM4
            /*buttonList[0xDC] = Pipe; */intDictionary.Add(Pipe.VirtualCode, Pipe); nameDictionary.Add(Pipe.Name, Pipe);          //OEM5
            /*buttonList[0xDD] = CloseBrackets; */intDictionary.Add(CloseBrackets.VirtualCode, CloseBrackets); nameDictionary.Add(CloseBrackets.Name, CloseBrackets); //OEM6
            /*buttonList[0xDE] = Quotes; */intDictionary.Add(Quotes.VirtualCode, Quotes); nameDictionary.Add(Quotes.Name, Quotes);        //OEM7
            ///*buttonList[0xDF] = OEM8; */intDictionary.Add(OEM8.VirtualCode, OEM8); buttonDictionary.Add(OEM8.Name, OEM8);
            ///*buttonList[0xE1] = OEMAX; */intDictionary.Add(OEMAX.VirtualCode, OEMAX); buttonDictionary.Add(OEMAX.Name, OEMAX);
            ///*buttonList[0xE2] = OEM102; */intDictionary.Add(OEM102.VirtualCode, OEM102); buttonDictionary.Add(OEM102.Name, OEM102);
            ///*buttonList[0xE3] = ICOHelp; */intDictionary.Add(ICOHelp.VirtualCode, ICOHelp); buttonDictionary.Add(ICOHelp.Name, ICOHelp);
            ///*buttonList[0xE4] = ICO00; */intDictionary.Add(ICO00.VirtualCode, ICO00); buttonDictionary.Add(ICO00.Name, ICO00);
            ///*buttonList[0xE5] = ProcessKey; */intDictionary.Add(ProcessKey.VirtualCode, ProcessKey); buttonDictionary.Add(ProcessKey.Name, ProcessKey); buttonDictionary.Add($3.Name, $3);$4
            ///*buttonList[0xE6] = ICOClear; */intDictionary.Add(ICOClear.VirtualCode, ICOClear); buttonDictionary.Add(ICOClear.Name, ICOClear);
            ///*buttonList[0xE7] = Packet; */intDictionary.Add(Packet.VirtualCode, Packet); buttonDictionary.Add(Packet.Name, Packet);
            ///*buttonList[0xE9] = OEMReset; */intDictionary.Add(OEMReset.VirtualCode, OEMReset); buttonDictionary.Add(OEMReset.Name, OEMReset);
            ///*buttonList[0xEA] = OEMJump; */intDictionary.Add(OEMJump.VirtualCode, OEMJump); buttonDictionary.Add(OEMJump.Name, OEMJump);
            ///*buttonList[0xEB] = OEMPA1; */intDictionary.Add(OEMPA1.VirtualCode, OEMPA1); buttonDictionary.Add(OEMPA1.Name, OEMPA1);
            ///*buttonList[0xEC] = OEMPA2; */intDictionary.Add(OEMPA2.VirtualCode, OEMPA2); buttonDictionary.Add(OEMPA2.Name, OEMPA2);
            ///*buttonList[0xED] = OEMPA3; */intDictionary.Add(OEMPA3.VirtualCode, OEMPA3); buttonDictionary.Add(OEMPA3.Name, OEMPA3);
            ///*buttonList[0xEE] = OEMWSCtrl; */intDictionary.Add(OEMWSCtrl.VirtualCode, OEMWSCtrl); buttonDictionary.Add(OEMWSCtrl.Name, OEMWSCtrl);
            ///*buttonList[0xEF] = OEMCUSel; */intDictionary.Add(OEMCUSel.VirtualCode, OEMCUSel); buttonDictionary.Add(OEMCUSel.Name, OEMCUSel);
            ///*buttonList[0xF0] = OEMATTN; */intDictionary.Add(OEMATTN.VirtualCode, OEMATTN); buttonDictionary.Add(OEMATTN.Name, OEMATTN);
            ///*buttonList[0xF1] = OEMFinish; */intDictionary.Add(OEMFinish.VirtualCode, OEMFinish); buttonDictionary.Add(OEMFinish.Name, OEMFinish);
            ///*buttonList[0xF2] = OEMCopy; */intDictionary.Add(OEMCopy.VirtualCode, OEMCopy); buttonDictionary.Add(OEMCopy.Name, OEMCopy);
            ///*buttonList[0xF3] = OEMAuto; */intDictionary.Add(OEMAuto.VirtualCode, OEMAuto); buttonDictionary.Add(OEMAuto.Name, OEMAuto);
            ///*buttonList[0xF4] = OEMENLW; */intDictionary.Add(OEMENLW.VirtualCode, OEMENLW); buttonDictionary.Add(OEMENLW.Name, OEMENLW);
            ///*buttonList[0xF5] = OEMBackTab; */intDictionary.Add(OEMBackTab.VirtualCode, OEMBackTab); buttonDictionary.Add(OEMBackTab.Name, OEMBackTab);
            ///*buttonList[0xF6] = ATTN; */intDictionary.Add(ATTN.VirtualCode, ATTN); buttonDictionary.Add(ATTN.Name, ATTN);
            ///*buttonList[0xF7] = CRSel; */intDictionary.Add(CRSel.VirtualCode, CRSel); buttonDictionary.Add(CRSel.Name, CRSel);
            ///*buttonList[0xF8] = EXSel; */intDictionary.Add(EXSel.VirtualCode, EXSel); buttonDictionary.Add(EXSel.Name, EXSel);
            ///*buttonList[0xF9] = EREOF; */intDictionary.Add(EREOF.VirtualCode, EREOF); buttonDictionary.Add(EREOF.Name, EREOF);
            ///*buttonList[0xFA] = Play; */intDictionary.Add(Play.VirtualCode, Play); buttonDictionary.Add(Play.Name, Play);
            ///*buttonList[0xFB] = Zoom; */intDictionary.Add(Zoom.VirtualCode, Zoom); buttonDictionary.Add(Zoom.Name, Zoom);
            ///*buttonList[0xFC] = Noname; */intDictionary.Add(Noname.VirtualCode, Noname); buttonDictionary.Add(Noname.Name, Noname);
            ///*buttonList[0xFD] = PA1; */intDictionary.Add(PA1.VirtualCode, PA1); buttonDictionary.Add(PA1.Name, PA1);
            ///*buttonList[0xFE] = OEMClear; */intDictionary.Add(OEMClear.VirtualCode, OEMClear); buttonDictionary.Add(OEMClear.Name, OEMClear);
            
            //Buttons = buttonList.ToList().AsReadOnly();
            ButtonIntLookup = new ReadOnlyDictionary<int, Button>(intDictionary);
            ButtonNameLookup = new ReadOnlyDictionary<string, Button>(nameDictionary);
        }
        #endregion
        #region String Lookup
        //TODO string lookup
        #endregion

        public override string ToString()
        {
            return virtualKey.ToString();
        }
    }
    public sealed class Switch
    {
        public event InputEventDelegate Hotkey;

        private readonly Button button;
        private readonly OutputKey outputKey;
        private readonly bool isUpSwitch;
        private readonly ManualResetEvent physicalEvt, logicalEvt;
        private Trigger physicalTrigger, logicalTrigger;

        public Button Button { get { return button; } }
        public int VirtualCode { get { return button.VirtualCode; } }
        public bool IsUpSwitch { get { return isUpSwitch; } }
        public bool IsDownSwitch { get { return !isUpSwitch; } }
        public bool IsPhysicallySet { get { return physicalEvt.WaitOne(0); } }
        public bool IsLogicallySet { get { return logicalEvt.WaitOne(0); } }
        public bool IsMouseSwitch { get { return button.IsMouseButton; } }
        public bool IsKeyboardSwitch { get { return button.IsKeyboardButton; } }
        internal OutputKey OutputKey { get { return outputKey; } }

        public Switch Sister { get { return isUpSwitch ? button.DownSwitch : button.UpSwitch; } }

        internal Switch(Button button, bool isUpSwitch, bool isSet)
        {
            this.button = button;
            this.isUpSwitch = isUpSwitch;
            this.physicalEvt = new ManualResetEvent(isSet);
            this.logicalEvt = new ManualResetEvent(isSet);
            this.physicalTrigger = logicalTrigger = null;
            this.outputKey = new OutputKey(this);
        }
        
        public Trigger PhysicalTrigger
        {
            get
            {
                Trigger newT, cur = physicalTrigger;
                if (cur != null)
                    return cur;

                cur = Interlocked.CompareExchange(ref physicalTrigger, newT = new Trigger(), null);
                if (cur == null)
                    return newT;
                return cur;
            }
        }
        public Trigger LogicalTrigger
        {
            get
            {
                Trigger newT, cur = logicalTrigger;
                if (cur != null)
                    return cur;

                cur = Interlocked.CompareExchange(ref logicalTrigger, newT = new Trigger(), null);
                if (cur == null)
                    return newT;
                return cur;
            }
        }

        public void ClearHotkeys() { Hotkey = null; }
        public bool Wait() { return physicalEvt.WaitOne(Timeout.Infinite); }
        public bool Wait(int timeout) { return physicalEvt.WaitOne(timeout); }
        public bool Wait(TimeSpan timeout) { return physicalEvt.WaitOne(timeout); }
        internal void Set(InputEventArgs inputEventArgs)
        {
            Hotkey?.Invoke(inputEventArgs);
            if (inputEventArgs.InputEvent.IsVirtual && !inputEventArgs.Handled)
            {
                Sister.logicalEvt.Reset();
                Trigger curTrigger = logicalTrigger;
                if (curTrigger != null)
                {
                    curTrigger.Set();
                    Interlocked.CompareExchange(ref logicalTrigger, null, curTrigger);
                }
                logicalEvt.Set();
            }
            else
            {
                Sister.physicalEvt.Reset();
                if(!inputEventArgs.Handled)
                    Sister.logicalEvt.Reset();
                Trigger curTrigger = physicalTrigger;
                if (curTrigger != null)
                {
                    curTrigger.Set();
                    Interlocked.CompareExchange(ref physicalTrigger, null, curTrigger);
                }
                if (!inputEventArgs.Handled)
                {
                    curTrigger = logicalTrigger;
                    if (curTrigger != null)
                    {
                        curTrigger.Set();
                        Interlocked.CompareExchange(ref logicalTrigger, null, curTrigger);
                    }
                }
                physicalEvt.Set();
                if (!inputEventArgs.Handled)
                    logicalEvt.Set();
            }
        }

        public override string ToString()
        {
            return button.ToString() + (isUpSwitch ? ":up" : ":down");
        } 
    }
    public sealed class Trigger
    {
        private bool isSet = false;
        public bool IsSet { get { return isSet; } }
        internal void Set() { isSet = true; }
        internal Trigger() { }
    }

    public delegate void InputEventDelegate(InputEventArgs args);
    public sealed class InputEventArgs
    {
        private readonly InputEvent inputEvent;
        private bool handled;

        public InputEvent InputEvent { get { return inputEvent; } }
        public bool Handled { get { return handled; } set { handled = value; } }

        private bool shift, ctrl, alt;
        public bool ShiftState { get { return shift; } }
        public bool CtrlState { get { return ctrl; } }
        public bool AltState { get { return alt; } }

        internal InputEventArgs(InputEvent inputEvent)
        {
            this.inputEvent = inputEvent;
            this.handled = false;
            this.shift = Button.LeftShift.DownSwitch.IsLogicallySet || Button.RightShift.DownSwitch.IsLogicallySet;
            this.ctrl = Button.LeftControl.DownSwitch.IsLogicallySet || Button.RightControl.DownSwitch.IsLogicallySet;
            this.alt = Button.LeftAlt.DownSwitch.IsLogicallySet || Button.RightAlt.DownSwitch.IsLogicallySet;
        }
    }
}
