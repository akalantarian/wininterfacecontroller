﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace InputController
{
    public static class UserSimulator
    {
        private static readonly Random rand = new Random();
        #region DLL Imports
        [DllImport("user32.dll")]
        private static extern UInt32 SendInput(int nInputs, [MarshalAs(UnmanagedType.LPArray), In] OutputKey[] pInputs, int cbSize);

        //[DllImport("kernel32.dll")]
        //static extern uint GetLastError();
        #endregion

        public static void SendAsync(Switch swtch) { Task.Factory.StartNew(() => Send(swtch)); }
        public static void SendAsync(Switch swtch, out ManualResetEventSlim evt) { ManualResetEventSlim evnt = evt = new ManualResetEventSlim(false); Task.Factory.StartNew(() => { Send(swtch); evnt.Set(); }); }
        public static void SendAsync(Switch swtch, Action<Switch> callback) { Task.Factory.StartNew(() => { Send(swtch); callback?.Invoke(swtch); }); }
        public static void Send(Switch swtch)
        {
            HotKeys.QueueVirtualCommand(swtch);
            lock (rand)
                SendInput(1, new OutputKey[] { swtch.OutputKey }, OutputKey.Size);
        }




        public static void SendAsync(OutputCluster cluster) { Task.Factory.StartNew(() => Send(cluster)); }
        public static void SendAsync(OutputCluster cluster, out ManualResetEventSlim evt) { ManualResetEventSlim evnt = evt = new ManualResetEventSlim(false); Task.Factory.StartNew(() => { Send(cluster); evnt.Set(); }); }
        public static void SendAsync(OutputCluster cluster, Action<OutputCluster> callback) { Task.Factory.StartNew(() => { Send(cluster); callback?.Invoke(cluster); }); }
        public static void Send(OutputCluster cluster)
        {
            foreach (OutputEvent evt in cluster.Events)
            {
                if (evt.type == 1) Thread.Sleep(evt.wait.WaitTime);
                else if (evt.type == 2) Thread.Sleep(rand.Next(evt.waitRandom.WaitTimeMin, evt.waitRandom.WaitTimeMax));
                else
                {
                    HotKeys.QueueVirtualCommand(evt.key.Switch);
                    lock (rand)
                        SendInput(1, new OutputKey[] { evt.key }, OutputKey.Size);
                }
            }
        }

        //public static void Send(OutputString outputString)
        //{
        //    foreach (OutputCluster cluster in outputString.Clusters)
        //    {
        //        HotKeys.QueueVirtualCommand(cluster);
        //        lock (rand)
        //            SendInput(cluster.numEvents, cluster.arr, OutputKey.Size);
        //        int waitTime = cluster.WaitTime;
        //        if (cluster.WaitTimeMax != 0)
        //            waitTime += rand.Next(cluster.WaitTimeMin, cluster.WaitTimeMax);
        //        if (waitTime > 40)
        //            Thread.Sleep(waitTime);
        //        else if(waitTime > 0)
        //        {
        //            DateTime target = DateTime.UtcNow + TimeSpan.FromMilliseconds(waitTime);
        //            while (DateTime.UtcNow < target) ;
        //        }
        //    }
        //}

        //public static void SendAsync(OutputString outputString)
        //{
        //    Task.Factory.StartNew(() => { Send(outputString); });
        //}
        //public static void SendAsync(OutputString outputString, out ManualResetEvent evt)
        //{
        //    ManualResetEvent evnt = new ManualResetEvent(false);
        //    evt = evnt;
        //    Task.Factory.StartNew(() => { Send(outputString); evnt.Set(); });
        //}
        //public static void SendAsync(OutputString outputString, Action<OutputString> callback)
        //{
        //    Task.Factory.StartNew(() => { Send(outputString); if (callback != null) callback(outputString); });
        //}
    }
}