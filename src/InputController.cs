﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace InputController
{
    public static class HotKeys
    {
        #region DLL Imports
        [DllImport("user32.dll")]
        private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelInputProc lpfn, IntPtr hMod, uint dwThreadId);

        //[DllImport("user32.dll")]
        //private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        //[DllImport("kernel32.dll")]
        //private static extern IntPtr GetModuleHandle(string lpModuleName);

        [DllImport("user32.dll")]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, int wParam, IntPtr lParam);

        private delegate IntPtr LowLevelInputProc(int nCode, int wParam, IntPtr lParam);

        //[DllImport("user32.dll")]
        //private static extern short GetKeyState(int virtualKeyCode);
        #endregion

        private static readonly LinkedList<Switch> virtualEvents = new LinkedList<Switch>();
        private static IntPtr KBHookHandle, MSHookHandle;
        private static int kBHookInstalled = 0, mSHookInstalled = 0;
        private static bool enableDebug = false;
        public static readonly object KeyboardSyncLock = new object(), MouseSyncLock = new object();
        public static bool EnableDebug { get => enableDebug; set { if(value && consoleTask == null) consoleTask = Task.Factory.StartNew(console, TaskCreationOptions.LongRunning); enableDebug = value; } }
        public static bool KBHookInstalled { get { return kBHookInstalled == 1; } }
        public static bool MSHookInstalled { get { return mSHookInstalled == 1; } }
        private static Task consoleTask;
        private static ConcurrentQueue<InputEvent> evts = new ConcurrentQueue<InputEvent>();
        private static AutoResetEvent logEvt = new AutoResetEvent(false);
        
        private static void console()
        {
            InputEvent inputEvent;
            while(true)
            {
                logEvt.WaitOne();
                while(evts.TryDequeue(out inputEvent))
                {
                    if(EnableDebug)
                    {
                        StringBuilder outputBuilder = new StringBuilder();
                        if(inputEvent.Switch.IsKeyboardSwitch) outputBuilder.Append("Keyboard "); else outputBuilder.Append("Mouse ");
                        outputBuilder.Append("Event:\t{");
                        if(inputEvent.IsPhysical) outputBuilder.Append("P"); else outputBuilder.Append("V");
                        if(inputEvent.IsDuplicate) outputBuilder.Append(",D");
                        outputBuilder.Append("}\t[" + inputEvent.Timestamp.ToString(@"MM/dd/yyyy, HH:mm:ss:fff") + "]\t <" + inputEvent.Switch.ToString() + ">");
                        Console.WriteLine(outputBuilder.ToString());
                    }
                }
            }
        }


        public static bool InstallKeyboardHook()
        {
            if (Interlocked.CompareExchange(ref kBHookInstalled, 1, 0) == 0)
            {
                Task.Factory.StartNew(() =>
                {
                    KBHookHandle = SetWindowsHookEx(13, KBHookDel, IntPtr.Zero, 0);
                    System.Windows.Forms.Application.Run();
                }, TaskCreationOptions.LongRunning);
                return true;
            }
            return false;
        }
        public static bool InstallMouseHook()
        {
            if (Interlocked.CompareExchange(ref mSHookInstalled, 1, 0) == 0)
            {
                Task.Factory.StartNew(() =>
                {
                    MSHookHandle = SetWindowsHookEx(14, MSHookDel, IntPtr.Zero, 0);
                    System.Windows.Forms.Application.Run();
                }, TaskCreationOptions.LongRunning);
                return true;
            }
            return false;
        }



        private static readonly LowLevelInputProc KBHookDel = (nCode, wParam, lParam) =>
            {
                if (nCode < 0)
                    return CallNextHookEx(KBHookHandle, nCode, wParam, lParam);

                lock(KeyboardSyncLock)
                {
                    InputEvent inputEvent;

                    if(!InputEvent.KeyboardFromHook(wParam, lParam, out inputEvent))                   //Cant decode
                        return CallNextHookEx(KBHookHandle, nCode, wParam, lParam);

                    lock(virtualEvents)                                                                //Is Virtual
                        if(virtualEvents.Remove(inputEvent.Switch))
                            inputEvent.FlagVirtual();
#if DEBUG
                    evts.Enqueue(inputEvent);
                    logEvt.Set();
#endif
                    InputEventArgs args = new InputEventArgs(inputEvent);
                    inputEvent.Switch.Set(args);
                    if(args.Handled)
                        return (IntPtr)1;

                    return CallNextHookEx(KBHookHandle, nCode, wParam, lParam);
                }
            };

        private static readonly LowLevelInputProc MSHookDel = (nCode, wParam, lParam) =>
            {
                if (nCode < 0)
                    return CallNextHookEx(MSHookHandle, nCode, wParam, lParam);

                if (wParam == 0x200)//mouse move evt
                    return CallNextHookEx(MSHookHandle, nCode, wParam, lParam);

                lock(MouseSyncLock)
                {
                    InputEvent inputEvent;

                    if(!InputEvent.MouseFromHook(wParam, lParam, out inputEvent))                   //Cant decode
                        return CallNextHookEx(MSHookHandle, nCode, wParam, lParam);

                    lock(virtualEvents)                                                                //Is Virtual
                        if(virtualEvents.Remove(inputEvent.Switch))
                            inputEvent.FlagVirtual();
#if DEBUG
                    evts.Enqueue(inputEvent);
                    logEvt.Set();
#endif
                    InputEventArgs args = new InputEventArgs(inputEvent);
                    inputEvent.Switch.Set(args);
                    if(args.Handled)
                        return (IntPtr)1;

                    return CallNextHookEx(MSHookHandle, nCode, wParam, lParam);
                }
            };

        //internal static void QueueVirtualCommand(OutputCluster cluster)
        //{
        //    lock (virtualEvents)
        //        for (int i = 0; i < cluster.numEvents; i++ )
        //            if ((cluster.arr[i].type == 1 && kBHookInstalled == 1) || (cluster.arr[i].type == 0 && mSHookInstalled == 1))
        //                virtualEvents.AddLast(cluster.arr[i]);
        //}
        internal static void QueueVirtualCommand(Switch swtc)
        {
            if ((swtc.IsKeyboardSwitch && kBHookInstalled == 1) || (swtc.IsMouseSwitch && mSHookInstalled == 1))
                lock (virtualEvents)
                    virtualEvents.AddLast(swtc);
        }
    }
}
