﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace InputController
{
    //public class OutputString
    //{
    //    internal readonly List<OutputCluster> Clusters;
    //    private bool isUserPossible = true, isClosed = true, retestClosed = false, retestPossible = false;
    //    private string decodedString = "";
    //    private bool reDecode = false;

    //    private void reset()
    //    {
    //        retestClosed = retestPossible = reDecode = true;
    //        decodedString = "";
    //    }

    //    public OutputString()
    //    {
    //        Clusters = new List<OutputCluster>();
    //    }
    //    public OutputString(OutputString otherString)
    //    {
    //        Clusters = new List<OutputCluster>(otherString.Clusters.Capacity);
    //        Clusters.AddRange(otherString.Clusters);
    //        reset();
    //    }
    //    private OutputString(List<OutputCluster> clusters)
    //    {
    //        Clusters = clusters;
    //        reset();
    //    }

    //    public void Add(OutputString otherString)
    //    {
    //        Clusters.AddRange(otherString.Clusters);
    //        reset();
    //    }

    //    #region Add String
    //    public void Add(string outputString)
    //    {
    //        Clusters.AddRange(decodeOutputString(outputString, false, 0, 0, 0, 0, 0, 0));
    //        reset();
    //    }
    //    public void Add(string outputString, bool includeUps)
    //    {
    //        Clusters.AddRange(decodeOutputString(outputString, includeUps, 0, 0, 0, 0, 0, 0));
    //        reset();
    //    }
    //    public void Add(string outputString, bool includeUps, int downTime, int upTime)
    //    {
    //        Clusters.AddRange(decodeOutputString(outputString, includeUps, downTime, 0, 0, upTime, 0, 0));
    //        reset();
    //    }
    //    public void Add(string outputString, bool includeUps, int downTimeMin, int downTimeMax, int upTimeMin, int upTimeMax)
    //    {
    //        Clusters.AddRange(decodeOutputString(outputString, includeUps, 0, downTimeMin, downTimeMax, 0, upTimeMin, upTimeMax));
    //        reset();
    //    }
    //    public void Add(string outputString, bool includeUps, int downTime, int downTimeMin, int downTimeMax, int upTime, int upTimeMin, int upTimeMax)
    //    {
    //        Clusters.AddRange(decodeOutputString(outputString, includeUps, downTime, downTimeMin, downTimeMax, upTime, upTimeMin, upTimeMax));
    //        reset();
    //    }
    //    #endregion

    //    #region Add Button
    //    public void Add(Button button)
    //    {
    //        Add(button.DownSwitch);
    //        Clusters.Last().Add(button.UpSwitch.OutputKey);
    //        reset();
    //    }
    //    public void Add(Button button, int downTime)
    //    {
    //        if (downTime < 0)
    //            throw new ArgumentOutOfRangeException();

    //        Add(button.DownSwitch);
    //        if (downTime != 0) Clusters.Last().WaitTime += downTime;
    //        Clusters.Add(new OutputCluster(button.UpSwitch.OutputKey));
    //        reset();
    //    }
    //    public void Add(Button button, int downTimeMin, int downTimeMax)
    //    {
    //        if (downTimeMin < 0 || downTimeMin > downTimeMax)
    //            throw new ArgumentOutOfRangeException();

    //        Add(button.DownSwitch);
    //        if (downTimeMin != 0) Clusters.Last().WaitTimeMin += downTimeMin;
    //        if (downTimeMax != 0) Clusters.Last().WaitTimeMax += downTimeMax;
    //        Clusters.Add(new OutputCluster(button.UpSwitch.OutputKey));
    //        reset();
    //    }
    //    public void Add(Button button, int downTime, int downTimeMin, int downTimeMax)
    //    {
    //        if (downTime < 0 || downTimeMin < 0 || downTimeMin > downTimeMax)
    //            throw new ArgumentOutOfRangeException();

    //        Add(button.DownSwitch);
    //        if (downTime != 0) Clusters.Last().WaitTime += downTime;
    //        if (downTimeMin != 0) Clusters.Last().WaitTimeMin += downTimeMin;
    //        if (downTimeMax != 0) Clusters.Last().WaitTimeMax += downTimeMax;
    //        Clusters.Add(new OutputCluster(button.UpSwitch.OutputKey));
    //        reset();
    //    }
    //    #endregion

    //    #region Add Switch
    //    public void Add(Switch swtch)
    //    {
    //        if (Clusters.Count == 0 || Clusters.Last().WaitTime > 0 || Clusters.Last().WaitTimeMax > 0)
    //            Clusters.Add(new OutputCluster(swtch.OutputKey));
    //        else
    //            Clusters.Last().Add(swtch.OutputKey);
    //        reset();
    //    }
    //    #endregion

    //    #region Add Wait
    //    public void Add(int milliseconds = 0)
    //    {
    //        if (milliseconds < 0)
    //            throw new ArgumentOutOfRangeException();

    //        if (Clusters.Count == 0)
    //            Clusters.Add(new OutputCluster(milliseconds));
    //        else
    //            if (milliseconds != 0) Clusters.Last().WaitTime += milliseconds;
    //        retestPossible = true;
    //        reDecode = true;
    //    }
    //    public void Add(int minMilliseconds = 0, int maxMilliseconds = 0)
    //    {
    //        if (minMilliseconds < 0 || minMilliseconds > maxMilliseconds)
    //            throw new ArgumentOutOfRangeException();

    //        if (Clusters.Count == 0)
    //            Clusters.Add(new OutputCluster(minMilliseconds, maxMilliseconds));
    //        else
    //        {
    //            if (minMilliseconds != 0) Clusters.Last().WaitTimeMin += minMilliseconds;
    //            if (maxMilliseconds != 0) Clusters.Last().WaitTimeMax += maxMilliseconds;
    //        }
    //        retestPossible = true;
    //        reDecode = true;
    //    }
    //    public void Add(int milliseconds = 0, int minMilliseconds = 0, int maxMilliseconds = 0)
    //    {
    //        if (milliseconds  < 0 || minMilliseconds < 0 || minMilliseconds > maxMilliseconds)
    //            throw new ArgumentOutOfRangeException();

    //        if (Clusters.Count == 0)
    //            Clusters.Add(new OutputCluster(milliseconds, minMilliseconds, maxMilliseconds));
    //        else
    //        {
    //            if (milliseconds != 0) Clusters.Last().WaitTime += milliseconds;
    //            if (minMilliseconds != 0) Clusters.Last().WaitTimeMin += minMilliseconds;
    //            if (maxMilliseconds != 0) Clusters.Last().WaitTimeMax += maxMilliseconds;
    //        }
    //        retestPossible = true;
    //        reDecode = true;
    //    }
    //    #endregion

    //    public bool IsUserPossible
    //    {
    //        get
    //        {
    //            if (!retestPossible)
    //                return isUserPossible;
    //            retestPossible = false;

    //            LinkedList<Button> downButtons = new LinkedList<Button>();
    //            foreach (OutputCluster cluster in Clusters)
    //            {
    //                if(cluster.numEvents >= 2)
    //                    return isUserPossible = false;
    //                for (int i = 0; i < cluster.numEvents; i++)
    //                {
    //                    OutputKey evt = cluster.arr[i];
    //                    if (evt.Switch.IsDownSwitch)
    //                    {
    //                        if(downButtons.Contains(evt.Switch.Button))
    //                            return isUserPossible = false;
    //                        downButtons.AddLast(evt.Switch.Button);
    //                    }
    //                    else
    //                    {
    //                        downButtons.Remove(evt.Switch.Button);
    //                    }
    //                }
    //            }
    //            return isUserPossible = true;
    //        }
    //    }
    //    public bool IsClosed
    //    {
    //        get
    //        {
    //            if (!retestClosed)
    //                return isClosed;
    //            retestClosed = false;

    //            LinkedList<Button> downButtons = new LinkedList<Button>();
    //            foreach (OutputCluster cluster in Clusters)
    //            {
    //                for (int i = 0; i < cluster.numEvents; i++)
    //                {
    //                    OutputKey evt = cluster.arr[i];
    //                    if (evt.Switch.IsDownSwitch)
    //                    {
    //                        downButtons.AddLast(evt.Switch.Button);
    //                    }
    //                    else if (!downButtons.Remove(evt.Switch.Button))
    //                        return isClosed = false;
    //                }
    //            }
    //            return isClosed = true;
    //        }
    //    }


    //    private static void processMatch(Match m, List<Button> brackets, List<OutputCluster> output)
    //    {
    //        if (m.Value[0] == '[')
    //        {
    //            string[] vals = m.Value.Substring(1, m.Value.Length - 2).Split(',');
    //            if (vals.Length == 1)
    //            {
    //                int waitTime;
    //                if (!int.TryParse(vals[0], out waitTime))
    //                    throw new ArgumentException();
    //                if (output.Count == 0)
    //                    output.Add(new OutputCluster(waitTime));
    //                else
    //                    output.Last().WaitTime += waitTime;
    //            }
    //            else if (vals.Length == 2)
    //            {
    //                int waitTimeMin, waitTimeMax;
    //                if (!int.TryParse(vals[0], out waitTimeMin) || !int.TryParse(vals[1], out waitTimeMax))
    //                    throw new ArgumentException();
    //                if (output.Count == 0 || output.Last().WaitTime != 0 || output.Last().WaitTimeMax != 0)
    //                    output.Add(new OutputCluster(waitTimeMin, waitTimeMax));
    //                else
    //                {
    //                    output.Last().WaitTimeMin = waitTimeMin;
    //                    output.Last().WaitTimeMax = waitTimeMax;
    //                }
    //            }
    //            else if (vals.Length == 3)
    //            {
    //                int waitTime, waitTimeMin, waitTimeMax;
    //                if (!int.TryParse(vals[0], out waitTime) || !int.TryParse(vals[1], out waitTimeMin) || !int.TryParse(vals[2], out waitTimeMax))
    //                    throw new ArgumentException();
    //                if (output.Count == 0 || output.Last().WaitTime != 0 || output.Last().WaitTimeMax != 0)
    //                    output.Add(new OutputCluster(waitTime, waitTimeMin, waitTimeMax));
    //                else
    //                {
    //                    output.Last().WaitTime = waitTime;
    //                    output.Last().WaitTimeMin = waitTimeMin;
    //                    output.Last().WaitTimeMax = waitTimeMax;
    //                }
    //            }
    //            else
    //                throw new ArgumentException();
    //        }
    //        else
    //        {
    //            if (m.Value == "}")
    //            {
    //                if (brackets.Count == 0)
    //                    throw new ArgumentException();
    //                Button button = brackets.Last();
    //                brackets.RemoveAt(brackets.Count - 1);
    //                if (output.Count == 0 || output.Last().WaitTime != 0 || output.Last().WaitTimeMax != 0)
    //                    output.Add(new OutputCluster(button.UpSwitch.OutputKey));
    //                else
    //                    output.Last().Add(button.UpSwitch.OutputKey);
    //            }
    //            else
    //            {
    //                bool isUp = m.Value.Last() == '}';
    //                string keyName = m.Value.Substring(0, m.Value.Length - 1);

    //                Button button;
    //                Button.ButtonDictionary.TryGetValue(keyName, out button);

    //                if (button == null)
    //                    throw new ArgumentException();

    //                if (isUp)
    //                {
    //                    brackets.Remove(button);
    //                    if (output.Count == 0 || output.Last().WaitTime != 0 || output.Last().WaitTimeMax != 0)
    //                        output.Add(new OutputCluster(button.UpSwitch.OutputKey));
    //                    else
    //                        output.Last().Add(button.UpSwitch.OutputKey);
    //                }
    //                else
    //                {
    //                    brackets.Add(button);
    //                    if (output.Count == 0 || output.Last().WaitTime != 0 || output.Last().WaitTimeMax != 0)
    //                        output.Add(new OutputCluster(button.DownSwitch.OutputKey));
    //                    else
    //                        output.Last().Add(button.DownSwitch.OutputKey);
    //                }
    //            }
    //        }
    //    }
    //    public static OutputString Decode(string formattedString)
    //    {
    //        if(formattedString == null)
    //            throw new ArgumentNullException();

    //        if (formattedString == string.Empty)
    //            return new OutputString();

    //        int target;
    //        List<Button> brackets = new List<Button>();
    //        List<OutputCluster> output = new List<OutputCluster>();
            
    //        Match m = Regex.Match(formattedString, @"\[[\d,]+?\]|[^\[\{\}]+?(?:\{|\})|\}", RegexOptions.Compiled);
    //        if(!m.Success || m.Index != 0)
    //            throw new ArgumentException();
    //        Console.WriteLine(m.Value);

    //        processMatch(m, brackets, output);

    //        while((target = m.Index + m.Length) < formattedString.Length)
    //        {
    //            m = m.NextMatch();
    //            if(!m.Success || m.Index != target)
    //                throw new ArgumentException();
    //            Console.WriteLine(m.Value);

    //            processMatch(m, brackets, output);
    //        }
    //        return new OutputString(output);
    //    }

    //    private static List<OutputCluster> decodeOutputString(string outputString, bool includeUps, int downTime, int downTimeMin, int downTimeMax, int upTime, int upTimeMin, int upTimeMax)
    //    {
    //        throw new NotSupportedException();//TODO string decoding
    //    }

    //    public string DecodedToString { get { return ToString(); } }
    //    public override string ToString()
    //    {
    //        if (!reDecode)
    //            return decodedString;
    //        reDecode = false;

    //        StringBuilder outputString = new StringBuilder();
    //        List<Button> brackets = new List<Button>();

    //        foreach (OutputCluster cluster in Clusters)
    //        {
    //            for (int i = 0; i < cluster.numEvents; i++)
    //            {
    //                Switch swtch = cluster.arr[i].Switch;
    //                if (swtch == null)
    //                    throw new Exception();

    //                if (swtch.IsUpSwitch)
    //                {
    //                    int bracketIDX;
    //                    if (brackets.Count != 0 && (bracketIDX = brackets.IndexOf(swtch.Button)) != -1)
    //                    {
    //                        brackets.RemoveAt(bracketIDX);
    //                        if (bracketIDX == brackets.Count)
    //                            outputString.Append('}');
    //                        else
    //                        {
    //                            outputString.Append(swtch.Button.VirtualKey.ToString() + "}");
    //                        }
    //                    }
    //                    else
    //                    {
    //                        outputString.Append(swtch.Button.VirtualKey.ToString() + "}");
    //                    }
    //                }
    //                else
    //                {
    //                    outputString.Append(swtch.Button.VirtualKey.ToString() + "{");
    //                    brackets.Add(swtch.Button);
    //                }
    //            }
    //            if (cluster.WaitTime != 0)
    //            {
    //                if (cluster.WaitTimeMax == 0)
    //                    outputString.Append("[" + cluster.WaitTime + "]");
    //                else
    //                    outputString.Append("[" + cluster.WaitTime + "," + cluster.WaitTimeMin + "," + cluster.WaitTimeMax + "]");
    //            }
    //            else if (cluster.WaitTimeMax != 0)
    //                outputString.Append("[" + cluster.WaitTimeMin + "," + cluster.WaitTimeMax + "]");
    //        }

    //        return decodedString =  outputString.ToString();
    //    }
    //}
    //internal class OutputCluster
    //{
    //    private const int initialSize = 1;
    //    public OutputKey[] arr;
    //    public int numEvents;
    //    public int WaitTime, WaitTimeMin, WaitTimeMax;

    //    public OutputCluster()
    //    {
    //        arr = new OutputKey[initialSize];
    //        numEvents = 0;
    //        WaitTime = WaitTimeMin = WaitTimeMax = 0;
    //    }
    //    public OutputCluster(OutputKey evt)
    //    {
    //        arr = new OutputKey[initialSize];
    //        numEvents = 1;
    //        WaitTime = WaitTimeMin = WaitTimeMax = 0;
    //        arr[0] = evt;
    //    }
    //    public OutputCluster(int waitTime)
    //    {
    //        arr = new OutputKey[initialSize];
    //        numEvents = 0;
    //        WaitTime = waitTime;
    //        WaitTimeMin = WaitTimeMax = 0;
    //    }
    //    public OutputCluster(int waitTimeMin, int waitTimeMax)
    //    {
    //        arr = new OutputKey[initialSize];
    //        WaitTime = 0;
    //        WaitTimeMin = waitTimeMin;
    //        WaitTimeMax = waitTimeMax;
    //    }
    //    public OutputCluster(int waitTime, int waitTimeMin, int waitTimeMax)
    //    {
    //        arr = new OutputKey[initialSize];
    //        WaitTime = waitTime;
    //        WaitTimeMin = waitTimeMin;
    //        WaitTimeMax = waitTimeMax;
    //    }
    //    public void Add(OutputKey evt)
    //    {
    //        if (arr.Length == numEvents)
    //        {
    //            OutputKey[] old = arr;
    //            arr = new OutputKey[old.Length * 2];
    //            old.CopyTo(arr, 0);
    //        }
    //        arr[numEvents++] = evt;
    //    }
    //}



    public class OutputCluster // Valid encoded strings: "<button name>{[<constant wait msec>][<random wait min>,<random wait max>]<button name>}"    ->     "Numpad1{[20][10,30]Numpad1}"
    {
        private List<OutputEvent> events = new List<OutputEvent>();

        internal IReadOnlyCollection<OutputEvent> Events { get { return events.AsReadOnly(); } }
        public bool IsPossible
        {
            get
            {
                bool keyEvent = false;
                foreach (OutputEvent evt in events)
                {
                    if(evt.type == 2)
                    {
                        if (keyEvent) return false;
                        keyEvent = true;
                    }
                    else
                    {
                        keyEvent = false;
                    }
                }
                return true;
            }
        }
        public bool IsComplete
        {
            get
            {
                LinkedList<Button> downKeys = new LinkedList<Button>();
                foreach (OutputEvent evt in events)
                {
                    if (evt.type == 2)
                    {
                        if (evt.key.Switch.IsDownSwitch)
                        {
                            if (!downKeys.Contains(evt.key.Switch.Button))
                                downKeys.AddLast(evt.key.Switch.Button);
                        }
                        else
                        {
                            if (!downKeys.Remove(evt.key.Switch.Button))
                                return false;
                        }
                    }
                }
                return downKeys.Count == 0;
            }
        }

        private void Add(OutputEvent evt)
        {
            if (!evt.Validate()) throw new Exception();
            events.Add(evt);
        }

        public void Add(int waitTime)
        {
            Add(new OutputEvent(new OutputWait(waitTime)));
        }
        public void Add(int waitMin, int waitMax)
        {
            Add(new OutputEvent(new OutputWaitRandom(waitMin, waitMax)));
        }
        public void Add(Switch key)
        {
            Add(new OutputEvent(key.OutputKey));
        }
        
        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            foreach(OutputEvent evt in events)
            {
                switch(evt.type)
                {
                    case 0: str.Append("["); str.Append(evt.wait.WaitTime); str.Append("]"); break;
                    case 1: str.Append("["); str.Append(evt.waitRandom.WaitTimeMin); str.Append(","); str.Append(evt.waitRandom.WaitTimeMax); str.Append("]"); break;
                    default:
                        str.Append(evt.key.Switch.Button.Name);
                        if (evt.key.Switch.IsDownSwitch)
                            str.Append("{");
                        else
                            str.Append("}");
                        break;
                }
            }
            return str.ToString();
        }

        //downKeyTime = time between "down" of one letter, and "up" of the same letter
        //betweenLetterTime = time between "up" of one letter, and "down" of the next
        //preShiftTime = time between "down" of left shift, and "down" of the first capital letter (more than 1 capital in a row will follow standard betweenLetterTime & downKeyTime)
        //postShiftTime = time between "up" of last capital letter, and "up" of left shift
        //if min and max are idendical, it will be a static wait time
        //if min and max are 0, wait time is ommitted
        public static OutputCluster TypeWord(string word, int downKeyTimeMin, int downKeyTimeMax, int betweenLetterTimeMin, int betweenLetterTimeMax, int preShiftTimeMin, int preShiftTimeMax, int postShiftTimeMin, int postShiftTimeMax)
        {
            try
            {
                List<OutputEvent> events = new List<OutputEvent>();
                bool shiftPressed = false;
                for (int i = 0; i < word.Length; i++)
                {
                    if (char.IsUpper(word[i]) || shiftSymbolChars.Contains(word[i]))
                    {
                        if (!shiftPressed)
                        {
                            events.Add(new OutputEvent(Button.LeftShift.DownSwitch.OutputKey));
                            shiftPressed = true;
                            addWait(events, preShiftTimeMin, preShiftTimeMax);
                        }
                        addLetter(events, word[i], true);
                        addWait(events, downKeyTimeMin, downKeyTimeMax);
                        addLetter(events, word[i], false);
                        if (i == word.Length - 1 || !char.IsUpper(word[i + 1]))
                        {
                            addWait(events, postShiftTimeMin, postShiftTimeMax);
                            events.Add(new OutputEvent(Button.LeftShift.UpSwitch.OutputKey));
                            shiftPressed = false;
                        }
                        if (i < word.Length - 1)
                            addWait(events, betweenLetterTimeMin, betweenLetterTimeMax);
                    }
                    else if (char.IsLower(word[i]) || char.IsDigit(word[i]) || symbolChars.Contains(word[i]))
                    {
                        addLetter(events, word[i], true);
                        addWait(events, downKeyTimeMin, downKeyTimeMax);
                        addLetter(events, word[i], false);
                        if (i < word.Length - 1)
                            addWait(events, betweenLetterTimeMin, betweenLetterTimeMax);
                    }
                    else
                        throw new Exception();
                }
                OutputCluster cluster = new OutputCluster();
                cluster.events = events;
                return cluster;
            }
            catch
            {
                return null;
            }
        }
        private static void addWait(List<OutputEvent> events, int min, int max)
        {
            if (min == 0 && max == 0) return;
            else if (min <= 0 || min > max) throw new Exception();
            else if (min == max) events.Add(new OutputEvent(new OutputWait(min)));
            else events.Add(new OutputEvent(new OutputWaitRandom(min, max)));
        }
        private static readonly char[] shiftSymbolChars = new char[] { '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '{', '}', '|', ':', '"', '<', '>', '?' };
        private static readonly char[] symbolChars = new char[] { '-', '=', '[', ']', '\\', ';', '\'', ',', '.', '/' };
        private static void addLetter(List<OutputEvent> events, char c, bool down)
        {
            Button button = null;
            if (char.IsUpper(c))
                button = Button.ButtonNameLookup[c.ToString()];//events.Add(new OutputEvent(Button.ButtonNameLookup[c.ToString()].DownSwitch.OutputKey));
            else if (char.IsLower(c))
                button = Button.ButtonNameLookup[char.ToUpper(c).ToString()];//events.Add(new OutputEvent(Button.ButtonNameLookup[char.ToUpper(c).ToString()].DownSwitch.OutputKey));
            else if (char.IsDigit(c))
                button = Button.ButtonNameLookup["N" + c.ToString()];//events.Add(new OutputEvent(Button.ButtonNameLookup["N" + c.ToString()].DownSwitch.OutputKey));
            else
                switch (c)
                {
                    case '`': case '~': button = Button.Tilde; break;
                    case '!': button = Button.N1; break;
                    case '@': button = Button.N2; break;
                    case '#': button = Button.N3; break;
                    case '$': button = Button.N4; break;
                    case '%': button = Button.N5; break;
                    case '^': button = Button.N6; break;
                    case '&': button = Button.N7; break;
                    case '*': button = Button.N8; break;
                    case '(': button = Button.N9; break;
                    case ')': button = Button.N0; break;
                    case '-': case '_': button = Button.Minus; break;
                    case '\\': case '|': button = Button.Pipe; break;
                    case '[': case '{': button = Button.OpenBrackets; break;
                    case ']': case '}': button = Button.CloseBrackets; break;
                    case ';': case ':': button = Button.Colon; break;
                    case '\'': case '"': button = Button.Quotes; break;
                    case ',': case '<': button = Button.Comma; break;
                    case '.': case '>': button = Button.Period; break;
                    case '/': case '?': button = Button.Question; break;
                    default: throw new Exception();
                }
            if (down)
                events.Add(new OutputEvent(button.DownSwitch.OutputKey));
            else
                events.Add(new OutputEvent(button.UpSwitch.OutputKey));
        }

        public static OutputCluster Parse(string str)
        {
            try { OutputCluster cluster = new OutputCluster(); cluster.events = parse(str); return cluster; } catch { return null; }
        }

        private static List<OutputEvent> parse(string str)
        {
            List<OutputEvent> events = new List<OutputEvent>();
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == '[')
                {
                    i++;
                    StringBuilder number = new StringBuilder();
                    while (char.IsDigit(str[i]))
                        number.Append(str[i++]);
                    if (str[i] == ']')
                        events.Add(new OutputEvent(new OutputWait(int.Parse(number.ToString()))));
                    else if (str[i] == ',')
                    {
                        i++;
                        int min = int.Parse(number.ToString());
                        number.Clear();
                        while (char.IsDigit(str[i]))
                            number.Append(str[i++]);
                        if (str[i] == ']')
                            events.Add(new OutputEvent(new OutputWaitRandom(min, int.Parse(number.ToString()))));
                        else
                            throw new FormatException();
                    }
                    else
                        throw new FormatException();
                    break;
                }
                else
                {
                    i++;
                    StringBuilder key = new StringBuilder();
                    while (str[i] != '{' && str[i] != '}')
                        key.Append(str[i++]);
                    Button button;
                    if (!Button.ButtonNameLookup.TryGetValue(key.ToString(), out button))
                        throw new FormatException();
                    if (str[i] == '{')
                        events.Add(new OutputEvent(button.DownSwitch.OutputKey));
                    else
                        events.Add(new OutputEvent(button.UpSwitch.OutputKey));
                }
            }
            return events;
        }
    }

    [StructLayout(LayoutKind.Explicit)]
    internal struct OutputEvent
    {
        [FieldOffset(0)]
        public uint type;
        [FieldOffset(4)]
        public OutputWait wait;
        [FieldOffset(4)]
        public OutputWaitRandom waitRandom;
        [FieldOffset(4)]
        public OutputKey key;

        public OutputEvent(OutputWait wait)
        {
            this.type = 0;
            this.waitRandom = default(OutputWaitRandom);
            this.key = default(OutputKey);
            this.wait = wait;
        }
        public OutputEvent(OutputWaitRandom waitRandom)
        {
            this.type = 1;
            this.wait = default(OutputWait);
            this.key = default(OutputKey);
            this.waitRandom = waitRandom;
        }
        public OutputEvent(OutputKey key)
        {
            this.type = 2;
            this.wait = default(OutputWait);
            this.waitRandom = default(OutputWaitRandom);
            this.key = key;
        }

        public bool Validate()
        {
            switch (type)
            {
                case 0: return wait.Validate();
                case 1: return waitRandom.Validate();
                case 2: return key.Validate();
                default: return false;
            }
        }
    }

    internal struct OutputWait
    {
        public int WaitTime;

        public OutputWait(int waitTime)
        {
            WaitTime = waitTime;
        }

        public bool Validate() { return WaitTime > 0; }
    }

    internal struct OutputWaitRandom
    {
        public int WaitTimeMin, WaitTimeMax;

        public OutputWaitRandom(int waitTimeMin, int waitTimeMax)
        {
            WaitTimeMin = waitTimeMin;
            WaitTimeMax = waitTimeMax;
        }

        public bool Validate() { return WaitTimeMin > 0 && WaitTimeMax > WaitTimeMin; }
    }

    internal struct OutputKey
    {
        public static readonly int Size = Marshal.SizeOf(typeof(OutputKey));

        public uint type;
        public InputUnion U;

        public OutputKey(Switch swtch)
        {
            type = swtch.IsKeyboardSwitch ? (uint)1 : 0;
            U = new InputUnion(swtch);
        }

        public Switch Switch
        {
            get
            {
                if (type == 1)
                {
                    Button button;
                    if (!Button.ButtonIntLookup.TryGetValue(U.ki.wVk, out button))
                        return null;

                    if (U.ki.dwFlags == 0)
                        return button.DownSwitch;
                    else if (U.ki.dwFlags == 2)
                        return button.UpSwitch;
                    return null;
                }
                else if (type == 0)
                {
                    switch (U.mi.dwFlags)
                    {
                        case 0x02:
                            return Button.LeftButton.UpSwitch;
                        case 0x04:
                            return Button.LeftButton.DownSwitch;
                        case 0x08:
                            return Button.RightButton.UpSwitch;
                        case 0x10:
                            return Button.RightButton.DownSwitch;
                        case 0x20:
                            return Button.MiddleButton.UpSwitch;
                        case 0x40:
                            return Button.MiddleButton.DownSwitch;
                        case 0x800:
                            if (U.mi.mouseData == 120)
                                return Button.MouseWheelV.UpSwitch;
                            else if (U.mi.mouseData == -120)
                                return Button.MouseWheelV.DownSwitch;
                            return null;
                        case 0x1000:
                            if (U.mi.mouseData == 120)
                                return Button.MouseWheelH.UpSwitch;
                            else if (U.mi.mouseData == -120)
                                return Button.MouseWheelH.DownSwitch;
                            return null;
                        case 0x100:
                            if (U.mi.mouseData == 1)
                                return Button.ExtraButton1.UpSwitch;
                            else if (U.mi.mouseData == 2)
                                return Button.ExtraButton2.UpSwitch;
                            return null;
                        case 0x80:
                            if (U.mi.mouseData == 1)
                                return Button.ExtraButton1.DownSwitch;
                            else if (U.mi.mouseData == 2)
                                return Button.ExtraButton2.DownSwitch;
                            return null;
                        default:
                            return null;
                    }
                }
                else
                    return null;
            }
        }

        public bool Validate() { return Switch != null; }

        #region SubStructs
        [StructLayout(LayoutKind.Explicit)]
        public struct InputUnion
        {
            [FieldOffset(0)]
            public MOUSEINPUT mi;
            [FieldOffset(0)]
            public KEYBDINPUT ki;

            public InputUnion(Switch swtch)
            {
                if (swtch.IsKeyboardSwitch)
                {
                    mi = new MOUSEINPUT();
                    ki = new KEYBDINPUT(swtch);
                }
                else
                {
                    ki = new KEYBDINPUT();
                    mi = new MOUSEINPUT(swtch);
                }
            }
        }

        public struct KEYBDINPUT
        {
            public short wVk;
            public short wScan;
            public uint dwFlags;
            public uint time;
            public UIntPtr dwExtraInfo;

            public KEYBDINPUT(Switch swtch)
            {
                wVk = (short)swtch.VirtualCode;
                wScan = 0;
                dwFlags = swtch.IsUpSwitch ? (uint)2 : 0;
                time = 0;
                dwExtraInfo = UIntPtr.Zero;
            }
        }

        public struct MOUSEINPUT
        {
            public int dx;
            public int dy;
            public int mouseData;
            public uint dwFlags;
            public uint time;
            public UIntPtr dwExtraInfo;

            public MOUSEINPUT(Switch swtch)
            {
                dx = dy = 0;
                time = 0;
                dwExtraInfo = UIntPtr.Zero;

                if (swtch.Button.VirtualKey == Button.VirtualKeys.MouseWheelV)
                {
                    dwFlags = 0x800;
                    if (swtch.IsUpSwitch)
                        mouseData = 120;
                    else
                        mouseData = -120;
                }
                else if (swtch.Button.VirtualKey == Button.VirtualKeys.MouseWheelH)
                {
                    dwFlags = 0x1000;
                    if (swtch.IsUpSwitch)
                        mouseData = 120;
                    else
                        mouseData = -120;
                }
                else if (swtch.Button.VirtualKey == Button.VirtualKeys.ExtraButton1)
                {
                    mouseData = 1;
                    if (swtch.IsUpSwitch)
                        dwFlags = 0x100;
                    else
                        dwFlags = 0x80;
                }
                else if (swtch.Button.VirtualKey == Button.VirtualKeys.ExtraButton2)
                {
                    mouseData = 2;
                    if (swtch.IsUpSwitch)
                        dwFlags = 0x100;
                    else
                        dwFlags = 0x80;
                }
                else
                {
                    mouseData = 0;
                    if (swtch.Button.VirtualKey == Button.VirtualKeys.LeftButton)
                    {
                        if (swtch.IsUpSwitch)
                            dwFlags = 0x2;
                        else
                            dwFlags = 0x4;
                    }
                    else if (swtch.Button.VirtualKey == Button.VirtualKeys.RightButton)
                    {
                        if (swtch.IsUpSwitch)
                            dwFlags = 0x8;
                        else
                            dwFlags = 0x10;
                    }
                    else if (swtch.Button.VirtualKey == Button.VirtualKeys.MiddleButton)
                    {
                        if (swtch.IsUpSwitch)
                            dwFlags = 0x20;
                        else
                            dwFlags = 0x40;
                    }
                    else throw new Exception();
                }
            }
        }
        #endregion
    }

    public struct InputEvent
    {
        private readonly Switch swtch;
        private readonly bool isDuplicate;
        private bool isVirtual;
        private readonly DateTime timestamp;
        private readonly Coordinate coordinate;

        public Switch Switch { get { return swtch; } }
        public bool IsDuplicate { get { return isDuplicate; } }
        public bool IsVirtual { get { return isVirtual; } }
        public bool IsPhysical { get { return !isVirtual; } }
        //public bool IsUp { get { return swtch.IsUpSwitch; } }
        //public bool IsDown { get { return !swtch.IsUpSwitch; } }
        public DateTime Timestamp { get { return timestamp; } }
        public Coordinate Coordinate { get { return coordinate; } }

        //public Button Button { get { return swtch.Button; } }
        //public Switch SisterSwitch { get { return swtch.Sister; } }
        //public Trigger SisterTrigger { get { return swtch.Sister.Trigger; } }
        
        private InputEvent(Switch swtch, bool isDuplicate, DateTime timestamp, Coordinate coordinate = null)
        {
            this.swtch = swtch;
            this.isDuplicate = isDuplicate;
            this.isVirtual = false;
            this.timestamp = timestamp;
            this.coordinate = coordinate;
        }

        internal void FlagVirtual() { isVirtual = true; }

        internal static bool KeyboardFromHook(int wParam, IntPtr lParam, out InputEvent evt)
        {
            int idx = Marshal.ReadInt32(lParam);
            Button button;
            if(!Button.ButtonIntLookup.TryGetValue(idx, out button))
            {
                evt = default(InputEvent);
                return false;
            }

            switch (wParam)
            {
                case 0x100: //WM_KEYDOWN
                case 0x104: //WM_SYSKEYDOWN(for alt)
                    evt = new InputEvent(button.DownSwitch, button.DownSwitch.IsLogicallySet, DateTime.UtcNow);
                    return true;
                case 0x101: //WM_KEYUP
                case 0x105: //WM_SYSKEYUP(for alt)
                    evt = new InputEvent(button.UpSwitch, button.UpSwitch.IsLogicallySet, DateTime.UtcNow);
                    return true;
                default:
                    evt = default(InputEvent);
                    return false;
            }
        }
        internal static bool MouseFromHook(int wParam, IntPtr lParam, out InputEvent evt)
        {
            switch (wParam)
            {
                case 0x201:
                    evt = new InputEvent(Button.LeftButton.DownSwitch, Button.LeftButton.DownSwitch.IsLogicallySet, DateTime.UtcNow, new Coordinate(Marshal.ReadInt32(lParam), Marshal.ReadInt32(lParam, 4)));
                    return true;
                case 0x202:
                    evt = new InputEvent(Button.LeftButton.UpSwitch, Button.LeftButton.UpSwitch.IsLogicallySet, DateTime.UtcNow, new Coordinate(Marshal.ReadInt32(lParam), Marshal.ReadInt32(lParam, 4)));
                    return true;
                case 0x204:
                    evt = new InputEvent(Button.RightButton.DownSwitch, Button.RightButton.DownSwitch.IsLogicallySet, DateTime.UtcNow, new Coordinate(Marshal.ReadInt32(lParam), Marshal.ReadInt32(lParam, 4)));
                    return true;
                case 0x205:
                    evt = new InputEvent(Button.RightButton.UpSwitch, Button.RightButton.UpSwitch.IsLogicallySet, DateTime.UtcNow, new Coordinate(Marshal.ReadInt32(lParam), Marshal.ReadInt32(lParam, 4)));
                    return true;
                case 0x207:
                    evt = new InputEvent(Button.MiddleButton.DownSwitch, Button.MiddleButton.DownSwitch.IsLogicallySet, DateTime.UtcNow, new Coordinate(Marshal.ReadInt32(lParam), Marshal.ReadInt32(lParam, 4)));
                    return true;
                case 0x208:
                    evt = new InputEvent(Button.MiddleButton.UpSwitch, Button.MiddleButton.UpSwitch.IsLogicallySet, DateTime.UtcNow, new Coordinate(Marshal.ReadInt32(lParam), Marshal.ReadInt32(lParam, 4)));
                    return true;
                case 0x20A:
                    int mouseData = Marshal.ReadInt32(lParam, 8);
                    if (mouseData > 0)
                        evt = new InputEvent(Button.MouseWheelV.UpSwitch, false, DateTime.UtcNow, new Coordinate(Marshal.ReadInt32(lParam), Marshal.ReadInt32(lParam, 4)));
                    else if (mouseData < 0)
                        evt = new InputEvent(Button.MouseWheelV.DownSwitch, false, DateTime.UtcNow, new Coordinate(Marshal.ReadInt32(lParam), Marshal.ReadInt32(lParam, 4)));
                    else
                    {
                        evt = default(InputEvent);
                        return false;
                    }
                    return true;
                case 0x20E:
                    if (Marshal.ReadInt32(lParam, 8) > 0)
                        evt = new InputEvent(Button.MouseWheelH.UpSwitch, false, DateTime.UtcNow, new Coordinate(Marshal.ReadInt32(lParam), Marshal.ReadInt32(lParam, 4)));
                    else
                        evt = new InputEvent(Button.MouseWheelH.DownSwitch, false, DateTime.UtcNow, new Coordinate(Marshal.ReadInt32(lParam), Marshal.ReadInt32(lParam, 4)));
                    return true;
                case 0x20B:
                    mouseData = Marshal.ReadInt32(lParam, 8);
                    if (mouseData == 1)
                        evt = new InputEvent(Button.ExtraButton1.DownSwitch, Button.ExtraButton1.DownSwitch.IsLogicallySet, DateTime.UtcNow, new Coordinate(Marshal.ReadInt32(lParam), Marshal.ReadInt32(lParam, 4)));
                    else if (mouseData == 2)
                        evt = new InputEvent(Button.ExtraButton2.DownSwitch, Button.ExtraButton2.DownSwitch.IsLogicallySet, DateTime.UtcNow, new Coordinate(Marshal.ReadInt32(lParam), Marshal.ReadInt32(lParam, 4)));
                    else
                    {
                        evt = default(InputEvent);
                        return false;
                    }
                    return true;
                case 0x20C:
                    mouseData = Marshal.ReadInt32(lParam, 8);
                    if (mouseData == 1)
                        evt = new InputEvent(Button.ExtraButton1.UpSwitch, Button.ExtraButton1.UpSwitch.IsLogicallySet, DateTime.UtcNow, new Coordinate(Marshal.ReadInt32(lParam), Marshal.ReadInt32(lParam, 4)));
                    else if (mouseData == 2)
                        evt = new InputEvent(Button.ExtraButton2.UpSwitch, Button.ExtraButton2.UpSwitch.IsLogicallySet, DateTime.UtcNow, new Coordinate(Marshal.ReadInt32(lParam), Marshal.ReadInt32(lParam, 4)));
                    else
                    {
                        evt = default(InputEvent);
                        return false;
                    }
                    return true;
                default:
                    evt = default(InputEvent);
                    return false;
            }
        }

        public override string ToString()
        {
            return "[" + timestamp + "] " + swtch.ToString() + (swtch.IsMouseSwitch ? "@" + coordinate.ToString() : string.Empty);
        }
    }

    public class Coordinate
    {
        private int xCoord, yCoord;
        public int X { get { return xCoord; } }
        public int Y { get { return yCoord; } }

        public Coordinate(int x, int y)
        {
            this.xCoord = x;
            this.yCoord = y;
        }

        public override string ToString()
        {
            return "X-" + xCoord + " Y-" + yCoord;
        }
    }
}